// leave off @2x/@3x
const images = {
  citscilogo: require('../Images/citsci-logo-2.png'),

  citloginBg: require('../Images/login-bg2.png'),
  mapmarker: require('../Images/mapmarker.png'),
  requestKit: require('../Images/Request-Kit.png'),
  beginSampling: require('../Images/Begin-Sampling.png'),

  logo: require('../Images/ir.png'),
  clearLogo: require('../Images/top_logo.png'),
  launch: require('../Images/launch-icon.png'),
  ready: require('../Images/your-app.png'),
  ignite: require('../Images/ignite_logo.png'),
  igniteClear: require('../Images/ignite-logo-transparent.png'),
  tileBg: require('../Images/tile_bg.png'),
  background: require('../Images/BG.png'),
  buttonBackground: require('../Images/button-bg.png'),
  api: require('../Images/Icons/icon-api-testing.png'),
  components: require('../Images/Icons/icon-components.png'),
  deviceInfo: require('../Images/Icons/icon-device-information.png'),
  faq: require('../Images/Icons/faq-icon.png'),
  home: require('../Images/Icons/icon-home.png'),
  theme: require('../Images/Icons/icon-theme.png'),
  usageExamples: require('../Images/Icons/icon-usage-examples.png'),
  chevronRight: require('../Images/Icons/chevron-right.png'),
  hamburger: require('../Images/Icons/hamburger.png'),
  backButton: require('../Images/Icons/back-button.png'),
  closeButton: require('../Images/Icons/close-button.png'),

  // Samples
  brighton: require('../Images/sample/brighton.jpg'),
  pointcook: require('../Images/sample/pointcook.jpg'),
  sorrento: require('../Images/sample/sorrento.jpg'),
  stkilda: require('../Images/sample/stkilda.jpg'),
  williamstown: require('../Images/sample/williamstown.jpg'),
  frankston: require('../Images/sample/frankston.jpg'),

}

export default images
