const colors = {  background: '#ffffff',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  semiTransparent: 'rgba(255,255,255,0.3)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#E0D7E5',
  red: '#D84315',

  border: "#ffffff",


  /* THEME */

  // statusbar: '#00ba6e',
  statusbar: '#005662',               // Updated colour (dark blue)
  backgroundLogin: '#00a361',

  darkText: '#333333',

  backgroundSwiper: '#00d17c',
  backgroundSwiperAlt: '#00ba6e',
  backgroundSwiperInfo: '#00d17c',
  backgroundSwiperActive: "#54DCA5",

  drawerContainer: '#007244',
  drawerLogoContainer: "#00BA6F",
  drawerButton: "#009257",

  buttonBackground: "#005662",

  primaryColor: '#00838f',
  primaryColorBorder: 'rgba(255,255,255,0.15)',
  primaryLightColor: '#4fb3bf',
  primaryDarkColor: '#005662',
  primaryDarkColorDisabled: '#adbabc',
  primaryDarkerColor: '#034851',
  primaryDarkerColorBorder: '#02343a',
  secondaryColor: '#546e7a',
  secondaryLightColor: '#79aaaf',
  secondaryLighterColor: '#83b5b7',
  secondaryDarkColor: '#29434e',
  onLightTextColor: '#333333',
  onLightTextColorMuted: '#888888',
  secondaryTextColor: '#595959',
  onDarkTextColor: '#ffffff',
  onDarkTextColorMuted: 'rgba(255,255,255,0.75)',

  borderColorLight: '#ccc',

  success: '#4BBF73',
  error: '#D33A36',
  muted: '#868e96',

}

export default colors
