import Fonts from './Fonts'
import Metrics from './Metrics'
import Colors from './Colors'

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.ricePaper,
    },

    background: {
      position: 'absolute',
      top: -5,
      bottom: 0,
      left: 0,
      right: 0,
      resizeMode: 'stretch',
    },
    contentContainer: {
        marginTop: 100,
        marginHorizontal: Metrics.spacing.xl,
    },
    topBar: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      flexDirection: 'row',
      alignItems: 'center',
      padding: Metrics.spacing.medium,
      zIndex: 1,
      flex: 1,
      // alignItems: 'stretch',
    },
    menuButtonContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      margin: 5,
      width: 50,
      // TEMP
      // borderColor: 'orange',
      // borderWidth: 1,
    },
    menuButton: {
      backgroundColor: Colors.primaryDarkColor,
      borderRadius: 3,
      elevation: 1,
      height: 50,
      elevation: 3,
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    topBarWithTitle: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      flexDirection: 'row',
      alignItems: 'center',
      padding: 5,
      zIndex: 1,
      flex: 1,
      // alignItems: 'stretch',
      backgroundColor: Colors.primaryDarkColor,
      elevation: 3,
    },
    menuButtonWithTitle: {
      backgroundColor: Colors.transparent,
      borderRadius: 3,
      elevation: 0,
      flex: 1,
    },
    centered: {
      textAlign: "center"
    },

    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    container: {
      flex: 1,
      paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.snow
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    subtitle: {
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: Fonts.size.normal,
      color: Colors.text
    }
  },
  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: 'center',
    textAlign: 'center'
  }
}

export default ApplicationStyles
