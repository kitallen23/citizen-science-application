import map from '../Utils/Map/map.reducers.js'
import { combineReducers, createStore, applyMiddleware } from 'redux';
import navigation from '../Utils/Navigation/navigation.reducers.js';
import settings from '../Utils/Settings/settings.reducers.js';
import sampling from '../Utils/Sampling/sampling.reducers.js';
import {createLogger} from 'redux-logger';

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    nav: require('./NavigationRedux').reducer,
    map,
    navigation,
    settings,
    sampling,
  })

  const middleware = applyMiddleware(
    createLogger(),
  );

  return createStore(rootReducer, middleware)
}
