import React, { Component } from 'react';
import { TouchableOpacity, Text, View, ScrollView } from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './Styles/HelpModalStyles';
import { Colors } from '../Themes/';

export default class HelpModal extends Component
{

  constructor(props){
    super(props);
  }

  render () {
    return (
      <View style={styles.mainWrapper}>
        <Modal
          isVisible={this.props.isVisible}
          animationIn={'slideInUp'}
          animationOut={'slideOutDown'}
          onBackdropPress={() => this.props.onClose()}
          onBackButtonPress={() => this.props.onClose()}>
          <View style={styles.dialog}>

            <View style={styles.dialogTitle}>
              <Text style={styles.dialogTitleText}>{this.props.title}</Text>
            </View>

            {/* TODO: Scroll view */}
            <View style={styles.dialogContentWrapper}>
              <ScrollView>
                <View style={styles.dialogContent}>
                  { this.props.children }
                </View>
              </ScrollView>
            </View>

            <View style={styles.buttonWrapper}>
              <View style={styles.dialogButton}>
                <TouchableOpacity onPress={() => this.props.onClose()}>
                  <Text style={styles.dialogButtonTextDark}>Close</Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>
        </Modal>

        <TouchableOpacity onPress={ () => this.props.onClick() }>
          <Text style={{textAlign: 'center'}}> <Icon name="help-circle-outline" size={this.props.iconSize} color={Colors.snow} /> </Text>
        </TouchableOpacity>
      </View>
  )}
}
