import React from 'react'
import PropTypes from 'prop-types'
import { TextInput } from 'react-native'
import styles from './Styles/TextInputLoginStyles'
import { Colors } from '../Themes'

export default class TextInputLogin extends TextInput
{

  constructor(props){
    super(props);
  }

  render () {

    return (
      <TextInput
        placeholderTextColor={Colors.secondaryLightColor}
        style={styles.input}
        underlineColorAndroid={'rgba(0,0,0,0)'}
        {...this.props}
      />
  )
  }
}
