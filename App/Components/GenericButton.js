import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RoundedButton from '../../App/Components/RoundedButton'
import styles from './Styles/GenericButtonStyles'

export default class GenericButton extends Component
{
  constructor(props){
    super(props);
  }

  render () {

    return (
        // Note: Fills the container it's in
        <RoundedButton
          onPress={this.props.onPress}
          text={this.props.text}

          style={styles.button}
          onSubmitEditing={this.props.onSubmitEditing}

        />
    )
  }
}
