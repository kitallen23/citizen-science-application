import React from 'react'
import { Image, Text, TouchableOpacity } from 'react-native'
import { Callout } from 'react-native-maps'
import styles from './Styles/SampleMapCalloutStyles'

import { Images } from '../Themes'

export default class SampleMapCallout extends React.Component {
  constructor (props) {
    super(props)
    this.onPress = this.props.onPress.bind(this, this.props.location)
  }

  render () {
    /* ***********************************************************
    * Customize the appearance of the callout that opens when the user interacts with a marker.
    * Note: if you don't want your callout surrounded by the default tooltip, pass `tooltip={true}` to `Callout`
    *************************************************************/
    const { location } = this.props
    return (
      <Callout style={styles.callout}>
        <TouchableOpacity onPress={this.onPress}>
          <Text>{location.title}</Text>
          {location.image ? (
            <Image source={location.image} key={location.image} style={styles.image} tooltip={true}/>
          ) : null}
        </TouchableOpacity>
      </Callout>
    )
  }
}
