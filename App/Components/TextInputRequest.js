import React from 'react'
import PropTypes from 'prop-types'
import { TextInput } from 'react-native'
import styles from './Styles/TextInputRequestStyles'

export default class TextInputRequest extends TextInput
{

  constructor(props){
    super(props);
  }

  render () {

    return (
      <TextInput
        style={styles.input}
        underlineColorAndroid={'rgba(0,0,0,0)'}
        {...this.props}
      />
  )
  }
}
