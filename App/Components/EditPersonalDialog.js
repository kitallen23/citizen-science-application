import React, { Component } from 'react';
import { AsyncStorage, TouchableOpacity, Text, View, Image, TextInput, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import Modal from 'react-native-modal';
import styles from './Styles/EditPersonalDialogStyles';
import { Colors } from '../Themes';

export default class EditPersonalDialog extends Component
{
  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({
      name: this.props.name,
      email: this.props.email,
    });

    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.setState({
          token: token,
          refresh_token: refresh_token
        });
      });
    });
  }

  dismissDialog() {
    this.setState({
      name: this.props.name,
      email: this.props.email,
    });
    this.props.onDismiss();
  }

  editDetails() {
    this.submitEditDetails();
  }

  submitEditDetails() {
    this.setState({loading:true});
    fetch('https://citsciapp.herokuapp.com/updateAccount', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        refresh_token: this.state.refresh_token,
        name: this.state.name,
        email: this.state.email,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);

      if (responseJson.status == "SUCCESS") {
        AsyncStorage.setItem("token", responseJson.token).then(() => {
        AsyncStorage.setItem("refresh_token", responseJson.refresh_token).then(() => {
        AsyncStorage.setItem("account:name", this.state.name).then(() => {
        AsyncStorage.setItem("account:email", this.state.email);
        });
        });
        });

        this.props.onSuccess(this.state.name, this.state.email);
        this.setState({loading:false});
        this.dismissDialog();
        return;
      }
      else {
        console.log(responseJSON.status);
      }
    })
    .catch((error) => {
      this.setState({loading:false});
      this.dismissDialog();
      console.error(error);
    })
  }

  render () {
    return (

        <Modal
          isVisible={this.props.show}
          animationIn={'slideInUp'}
          animationOut={'slideOutDown'}
          onBackdropPress={() => this.dismissDialog()}
          onBackButtonPress={() => this.dismissDialog()}
          ref={(postalDialog) => { this.postalDialog = postalDialog; }} >

          { this.state.loading ? (
            <View style={styles.dialogLoadingWrapper}>
                <ActivityIndicator size={60} color={Colors.snow}/>
            </View>
          ) : (
            <View style={styles.dialog}>

              <View style={styles.dialogTitle}>
                <Text style={styles.dialogTitleText}>Edit personal details</Text>
              </View>

              <View style={styles.dialogContent}>
                <TextInput
                  placeholder="Name"
                  style={styles.textInputEdit}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  keyboardType="default"
                  returnKeyType="next"
                  autoCapitalize="sentences"
                  value={this.state.name}
                  onChangeText={(name) => this.setState({name})}

                  onSubmitEditing={(event) => {
                    this.refs.EmailInput.focus();
                    }}
                />

                <TextInput
                  placeholder="Email"
                  style={styles.textInputEdit}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  keyboardType="email-address"
                  returnKeyType="next"
                  value={this.state.email}
                  onChangeText={(email) => this.setState({email})}
                  returnKeyType="done"

                  ref="EmailInput"
                />
              </View>

              <View style={styles.buttonWrapper}>

                <View style={styles.dialogButtonLeft}>
                  <TouchableOpacity onPress={() => this.dismissDialog()}>
                    <Text style={styles.dialogButtonTextDark}>Close</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.dialogButtonRight}>
                  <TouchableOpacity onPress={() => this.editDetails()}>
                    <Text style={styles.dialogButtonText}>Save</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          )}
        </Modal>
    )
  }
}
