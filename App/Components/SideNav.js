import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Image, TouchableOpacity, View, Text } from 'react-native'
import styles from './Styles/SideNavStyles'
import Icon from 'react-native-vector-icons/Ionicons';
import Drawer from 'react-native-drawer';
import { Colors, Images } from '../Themes/'

export default class SideNav extends Component
{

  constructor(props) {
    super(props);
  }

  closeNav() {
    this.props.closeAction()
  }

  changeContent(content) {
    this.props.changeContent(content)
  }

  render () {
    const content = this.props.currentContent

    return (
      <Drawer
        acceptPan={false}
        type="overlay"
        content={
          <View style={styles.drawerContainer}>

            <View style={styles.navLogoContainer}>
              <Image source={Images.citscilogo} style={styles.navLogo} />
            </View>

            <TouchableOpacity style={styles.navButton} onPress={ () => this.changeContent("Profile") }>
              <View style={styles.navButtonContent}>
                {content == "Profile" ?
                  (
                    <Icon  name="ios-contact" style={styles.navButtonIcon}  size={30} color={Colors.snow} />
                  ) : (
                    <Icon  name="ios-contact-outline" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  )
                }
                <Text style={styles.navText}>My Profile</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.navButton} onPress={ () => this.changeContent("MySamples") }>
              <View style={styles.navButtonContent}>
                {content == "Map" || content == "List" || content == "MySamples" ?
                  (
                    <Icon  name="ios-map" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  ) : (
                    <Icon  name="ios-map-outline" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  )
                }
                <Text style={styles.navText}>My Samples</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.navButton} onPress={ () => this.changeContent("Scan") }>
              <View style={styles.navButtonContent}>
                {content == "Scan" ?
                  (
                    <Icon  name="ios-barcode" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  ) : (
                    <Icon  name="ios-barcode-outline" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  )
                }
                <Text style={styles.navText}>View Sample</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.navButton} onPress={ () => this.changeContent("Settings") }>
              <View style={styles.navButtonContent}>
                {content == "Settings" ?
                  (
                    <Icon  name="ios-settings" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  ) : (
                    <Icon  name="ios-settings-outline" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  )
                }
                <Text style={styles.navText}>Settings</Text>
              </View>
            </TouchableOpacity>


            <TouchableOpacity style={styles.navButton} onPress={ () => this.changeContent("Help") }>
              <View style={styles.navButtonContent}>
                {content == "Help" ?
                  (
                    <Icon  name="ios-help-circle" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  ) : (
                    <Icon  name="ios-help-circle-outline" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                  )
                }
                <Text style={styles.navText}>Help</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.navButton} onPress={ () => this.changeContent("About") }>
              <View style={styles.navButtonContent}>
              {/*}{content == "About" ?
                (
                  <Icon name="ios-information-circle" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                ) : (
                  <Icon name="ios-information-circle-outline" style={styles.navButtonIcon} size={30} color={Colors.snow} />

                )
              } --> */}
                <Text style={styles.navText}>About</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.navButton} onPress={ () => this.changeContent("Legal") }>
              <View style={styles.navButtonContent}>
              {/* }{content == "Legal" ?
                (
                  <Icon name="ios-information-circle" style={styles.navButtonIcon} size={30} color={Colors.snow} />
                ) : (
                  <Icon name="ios-information-circle-outline" style={styles.navButtonIcon} size={30} color={Colors.snow} />

                )
              } */}
                <Text style={styles.navText}>Legal</Text>
              </View>
            </TouchableOpacity>

          </View>
        }
        tapToClose={true}
        openDrawerOffset={0.4} // 50% gap on the right side of drawer
        closedDrawerOffset={0}
        elevation={3}
        panOpenMask={50}
        tweenDuration={250}
        tweenEasing="easeOutQuad"
        type="displace"
        {...this.props}
        >
          {this.props.children}
      </Drawer>
    )
  }
}
