import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import styles from './Styles/PageTitleStyles'

export default class PageTitle extends Component
{

  constructor(props){
    super(props);
  }

  render () {
    return (
      <View style={styles.pageTitleContainer}>
        <Text
          style={styles.titleText}
        >
          {this.props.title}
        </Text>
      </View>
  )}
}
