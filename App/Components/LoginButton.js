import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RoundedButton from '../../App/Components/RoundedButton'
import styles from './Styles/LoginButtonStyles'

export default class LoginButton extends Component
{
  constructor(props){
    super(props);
  }

  render () {

    return (
        <RoundedButton
          onPress={this.props.onPress}
          text="Login"
          
          style={styles.button}
          onSubmitEditing={this.props.onSubmitEditing}

          />
    )
  }
}
