import React, { Component } from 'react';
import { AsyncStorage, TouchableOpacity, Text, View, Image, TextInput, KeyboardAvoidingView, Picker, ActivityIndicator } from 'react-native';
import Modal from 'react-native-modal';
import styles from './Styles/EditPostalDialogStyles';
import { Colors } from '../Themes';

export default class EditPesonalDialog extends Component
{
  constructor(props){
    super(props);
    this.state = {
      address: {
        streetAddress: "",
        city: "",
        postcode: "",
        state: "",
      },
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({
      address: this.props.address,
    });

    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.setState({
          token: token,
          refresh_token: refresh_token
        });
      });
    });
  }

  dismissDialog() {
    this.setState({address: this.props.address});
    this.props.onDismiss();
  }

  editDetails() {
    this.submitEditDetails();
  }

  submitEditDetails() {
    this.setState({loading:true});
    fetch('https://citsciapp.herokuapp.com/updateAddress', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        refresh_token: this.state.refresh_token,
        streetAddress: this.state.address.streetAddress,
        city: this.state.address.city,
        postcode: this.state.address.postcode,
        state: this.state.address.state,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      if (responseJson.status == "SUCCESS") {
        AsyncStorage.setItem("token", responseJson.token).then(() => {
        AsyncStorage.setItem("refresh_token", responseJson.refresh_token).then(() => {
        AsyncStorage.setItem("account:streetAddress", this.state.address.streetAddress).then((streetAddress) => {
        AsyncStorage.setItem("account:city", this.state.address.city).then((city) => {
        AsyncStorage.setItem("account:postcode", this.state.address.postcode).then((postcode) => {
        AsyncStorage.setItem("account:state", this.state.address.state);
        });
        });
        });
        });
        });

        this.props.onSuccess(this.state.address);
        this.setState({loading:false});
        this.dismissDialog();
        return;
      }
      else {
        console.log(responseJson.status);
        this.setState({loading:false});
        this.dismissDialog();
      }
    })
    .catch((error) => {
      this.setState({loading:false});
      this.dismissDialog();
      console.error(error);
    })
  }

  render () {
    return (

        <Modal
          isVisible={this.props.show}
          animationIn={'slideInUp'}
          animationOut={'slideOutDown'}
          onBackdropPress={() => this.dismissDialog()}
          onBackButtonPress={() => this.dismissDialog()}
          ref={(postalDialog) => { this.postalDialog = postalDialog; }} >

          { this.state.loading ? (
            <View style={styles.dialogLoadingWrapper}>
                <ActivityIndicator size={60} color={Colors.snow}/>
            </View>
          ) : (
            <View style={styles.dialog}>

              <View style={styles.dialogTitle}>
                <Text style={styles.dialogTitleText}>Edit personal details</Text>
              </View>

              <View style={styles.dialogContent}>


                {/*** Street Address ***/}
                <TextInput
                  keyboardType="default"
                  placeholder="Street Address"
                  style={styles.textInputEdit}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(streetAddress) => this.setState({address: {...this.state.address, streetAddress: streetAddress}})}
                  value={this.state.address.streetAddress}
                  returnKeyType="next"

                  ref='StreetAddress'

                  onSubmitEditing={(event) => {
                    this.refs.City.focus();
                  }}

                />

                {/*** City ***/}
                <TextInput
                  keyboardType="default"
                  placeholder="City"
                  style={styles.textInputEdit}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={(city) => this.setState({address: {...this.state.address, city: city}})}
                  value={this.state.address.city}
                  returnKeyType="next"

                  ref='City'

                  onSubmitEditing={(event) => {
                    this.refs.Postcode.focus();
                  }}

                />

                <View style={styles.formRow}>

                  {/*** Postcode ***/}
                  <View style={styles.formRowElement}>
                    <TextInput
                      keyboardType="numeric"
                      placeholder="Postcode"
                      style={styles.textInputLeft}
                      underlineColorAndroid={'rgba(0,0,0,0)'}
                      onChangeText={(postcode) => this.setState({address: {...this.state.address, postcode: postcode}})}
                      value={this.state.address.postcode}
                      returnKeyType="done"

                      ref='Postcode'

                    />
                  </View>

                  {/*** State ***/}
                  <View style={styles.formRowElement}>
                    <View style={styles.statePickerWrapper}>
                      <Picker style={styles.statePicker}
                        selectedValue={this.state.address.state}
                        onValueChange={(value) => this.setState({address: {...this.state.address, state: value}})}
                        ref='StateTerritory'>

                        <Picker.Item label="ACT" value="ACT" />
                        <Picker.Item label="NSW" value="NSW" />
                        <Picker.Item label="NT" value="NT" />
                        <Picker.Item label="QLD" value="QLD" />
                        <Picker.Item label="SA" value="SA" />
                        <Picker.Item label="TAS" value="TAS" />
                        <Picker.Item label="VIC" value="VIC" />
                        <Picker.Item label="WA" value="WA" />
                      </Picker>
                    </View>
                  </View>

                </View>
              </View>

              <View style={styles.buttonWrapper}>

                <View style={styles.dialogButtonLeft}>
                  <TouchableOpacity onPress={() => this.dismissDialog()}>
                    <Text style={styles.dialogButtonTextDark}>Close</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.dialogButtonRight}>
                  <TouchableOpacity onPress={ () => this.editDetails() }>
                    <Text style={styles.dialogButtonText}>Save</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          )}
        </Modal>
    )
  }
}
