import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './Styles/SampleCategoryStyles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Metrics, ApplicationStyles, Colors, Fonts } from '../Themes/';

export default class PageTitle extends Component
{
  constructor(props){
    super(props);
  }

  render () {
    return (
      <View style={styles.categoryWrapper}>
        {!this.props.isFilled ? (
          <TouchableOpacity onPress={this.props.onClick}>
            <View style={styles.categoryTitleWrapper}>

              <Text style={styles.categoryTitleText}>{this.props.title}</Text>

              <TouchableOpacity onPress={this.props.onClick}>
                <Icon name="plus" size={24} color={Colors.snow} />
              </TouchableOpacity>

            </View>
          </TouchableOpacity>
        ) : (
          <View style={styles.categoryTitleWrapper}>

            <TouchableOpacity onPress={this.props.toggleDisplay}>
              <Text style={styles.categoryTitleText}>{this.props.title}</Text>
            </TouchableOpacity>

            <View style={styles.buttonRow}>
              <TouchableOpacity onPress={this.props.onClick} style={{marginRight: 6}}>
                <Icon name="border-color" style={styles.navButtonIcon} size={22} color={Colors.snow} />
              </TouchableOpacity>
              <Icon name="check" size={24} color={Colors.success} />
            </View>

          </View>
        )}
        {this.props.isExpanded ? this.props.children : null }
      </View>
  )}
}
