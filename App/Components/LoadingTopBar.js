import React, { Component } from 'react'
import { ActivityIndicator, View, Text } from 'react-native'
import PropTypes from 'prop-types'
import styles from './Styles/LoadingTopBarStyles'

export default class LoadingTopBar extends Component
{

  constructor(props){
    super(props);
  }

  render () {
    return (
      <View style={styles.loadingTopBar}>
        <ActivityIndicator size={this.props.size ? this.props.size : 30} color={this.props.color ? this.props.color : null}/>
      </View>
  )}
}
