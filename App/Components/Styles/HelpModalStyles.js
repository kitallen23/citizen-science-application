import { StyleSheet, Dimensions } from 'react-native';
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/';
let modalHeight = 400;
let barHeight = 44;

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  // Modal styles
  dialog: {
    borderRadius: 5,
    backgroundColor: Colors.onDarkTextColor,
    height: modalHeight,
  },
  dialogTitle: {
    backgroundColor: '#F9F9FB',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    height: barHeight,
  },
  dialogTitleText: {
    fontSize: Fonts.size.large,
    fontWeight: 'bold',
    color: '#7F7D89',
    marginVertical: Metrics.baseMargin,
    textAlign: 'center',
  },
  dialogContentWrapper: {
    height: modalHeight - (2 * barHeight),
  },
  dialogContent: {
    padding: 20,
  },
  buttonWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    height: barHeight,
  },
  dialogButton: {
    flexGrow: 1,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F9F9FB',
  },
  dialogButtonTextDark: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
    color: '#7F7D89',
  },

  mainWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
