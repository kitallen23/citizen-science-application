import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  drawerContainer: {
    backgroundColor: Colors.primaryColor,
    width: "100%",
    height: "100%",
    padding: 10,
  },
  navLogoContainer: {
    padding: 15,
    backgroundColor: Colors.primaryColor,
    borderColor: Colors.primaryColor,
    borderBottomWidth: 1,
  },
  navLogo: {
    resizeMode: "contain",
    width: "100%",
    height: 100,
  },
  navButton: {
    padding: 5,
    backgroundColor: Colors.primaryColor,
    borderColor: Colors.primaryLightColor,
    borderBottomWidth: 1,
  },

  navSecondaryButton: {
    padding: 5,
    backgroundColor: Colors.primaryColor,
    borderColor: Colors.snow,
    borderBottomWidth: 1,
  },
  navButtonContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  navButtonIcon: {
    margin: 5,
  },

  navText: {
    margin: 5,
    fontWeight: 'bold',
    color: Colors.onDarkTextColor,
    fontSize: Fonts.size.normal,
  },
})
