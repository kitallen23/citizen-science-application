import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  button: {
    height: 45,
    borderRadius: 5,
    marginHorizontal: Metrics.loginItems,
    marginVertical: Metrics.baseMargin,
    backgroundColor: Colors.primaryDarkColor,
    justifyContent: 'center',
    borderColor: Colors.transparent,
    borderWidth: 2,
    padding: 20
  },
  buttonText: {
    color: Colors.snow,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
  }
})
