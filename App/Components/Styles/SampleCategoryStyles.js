import { StyleSheet } from 'react-native';
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  categoryWrapper: {
    // borderBottomColor: Colors.primaryDarkerColorBorder,
    borderBottomColor: Colors.primaryColorBorder,
    borderBottomWidth: 1,
    minHeight: 70,
  },
  categoryTitleWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: 'center',
  },
  categoryTitleText: {
    paddingVertical: 20,
    fontSize: Fonts.size.h5,
    color: Colors.onDarkTextColor,
  },
  buttonRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  }
})
