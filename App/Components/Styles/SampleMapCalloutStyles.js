import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  callout: {
    position: 'relative',
    alignItems: 'center',
    paddingBottom: 5,
    flex: 1,
  },
  image: {
  	width: 150,
  	height: 150,
  	resizeMode: "cover",
    borderRadius: 10,
  }
})
