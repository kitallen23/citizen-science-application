import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  button: {
    height: 45,
    borderRadius: 5,
    marginVertical: Metrics.baseMargin,
    backgroundColor: Colors.primaryDarkColor,
    justifyContent: 'center',
    borderColor: Colors.transparent,
    borderWidth: 2,
    padding: 20,
    flex: 1,
  },
})
