import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  titleText: {
    fontSize: Fonts.size.h5,
    fontWeight: 'bold',
    color: Colors.snow,
  },
  pageTitleContainer: {
		justifyContent: 'center',
    flex: 4,
  }
})
