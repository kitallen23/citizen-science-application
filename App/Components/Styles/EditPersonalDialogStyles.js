import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  dialog: {
    borderRadius: 5,
    backgroundColor: Colors.onDarkTextColor,
    height: 260,
  },
  dialogTitle: {
    backgroundColor: '#F9F9FB',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  dialogTitleText: {
    fontSize: Fonts.size.large,
    fontWeight: 'bold',
    color: '#7F7D89',
    marginVertical: Metrics.baseMargin,
    textAlign: 'center',
  },
  dialogContent: {
    padding: 20,
  },
  buttonWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  dialogButtonLeft: {
    flexGrow: 1,
    borderBottomLeftRadius: 5,
    backgroundColor: '#F9F9FB',
  },
  dialogButtonRight: {
    flexGrow: 1,
    backgroundColor: Colors.buttonBackground,
    borderBottomRightRadius: 5,
  },
  dialogButtonText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
    color: Colors.snow,
  },
  dialogButtonTextDark: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
    color: '#7F7D89',
  },
  textInputEdit: {
    height: 45,
    borderColor: Colors.borderColorLight,
    borderWidth: 1,
    borderRadius: 5,
    paddingRight: 20,
    paddingLeft: 20,
    marginBottom: 10,
    marginTop: 10,
    marginHorizontal: Metrics.section,
    fontSize: 20,
  },
})
