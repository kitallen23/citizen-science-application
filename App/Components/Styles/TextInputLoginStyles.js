import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
    input:
    {
        backgroundColor: Colors.snow,
        height: 45,
        borderColor: Colors.transparent,
        borderRadius: 5,
        borderWidth: 2,
        paddingRight: 20,
        paddingLeft: 20,
        marginBottom: 10,
        marginTop: 10,
        marginHorizontal: Metrics.loginItems,
        fontSize: 15,
    }
})
