import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  input:
  {
    height: 45,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    color: 'white',
    paddingRight: 20,
    paddingLeft: 20,
    marginBottom: 10,
    marginTop: 10,
    // marginHorizontal: Metrics.section,
    fontSize: 20,
  }
})
