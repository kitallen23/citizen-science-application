
// TODO: use this for all navigation!
import { StackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import SwiperScreen from '../Containers/SwiperScreen'
import MainScreen from '../Containers/mainScreen'
import RegistrationScreen from '../Containers/RegistrationScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  LaunchScreen: { screen: LaunchScreen },
  SwiperScreen: { screen: SwiperScreen },
  MainScreen: { screen: MainScreen },
  RegistrationScreen: { screen: RegistrationScreen },
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'MainScreen',
  navigationOptions: {
    headerStyle: styles.header
  },
})

export default PrimaryNav
