export const changePosition = location => ({
  type: 'CHANGE_POSITION',
  payload: location
});
