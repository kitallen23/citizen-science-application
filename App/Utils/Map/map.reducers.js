const initialState = {
  position: {latitude: -37.814263, longitude: 144.961014, latitudeDelta: 0.2, longitudeDelta: 0.2}
};

export default ( state = initialState, { type, payload }) => {
  switch ( type ) {
    case 'CHANGE_POSITION':
      return {
        ...state,
        position: payload
      };

    default:
      return state;
  }
};
