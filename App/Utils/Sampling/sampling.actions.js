export const changeSampleView = screen => ({
  type: 'CHANGE_SAMPLE_VIEW',
  payload: screen
});

export const changeSampleTitle = title => ({
  type: 'CHANGE_SAMPLE_TITLE',
  payload: title
});

export const changeSampleCode = code => ({
  type: 'CHANGE_SAMPLE_CODE',
  payload: code
});

export const changeSampleDatetime = datetime => ({
  type: 'CHANGE_SAMPLE_DATETIME',
  payload: datetime
});

export const changeSampleLocation = location => ({
  type: 'CHANGE_SAMPLE_LOCATION',
  payload: location
});

export const changeSamplePhotosAmount = photosAmount => ({
  type: 'CHANGE_SAMPLE_PHOTOS_AMOUNT',
  payload: photosAmount
});

export const changeSampleNotesText = text => ({
  type: 'CHANGE_SAMPLE_NOTES_TEXT',
  payload: text
});

export const toggleSampleCodeExpanded = () => ({
  type: 'TOGGLE_SAMPLE_CODE_EXPANDED'
});

export const toggleSampleDatetimeExpanded = () => ({
  type: 'TOGGLE_SAMPLE_DATETIME_EXPANDED'
});

export const toggleSampleLocationExpanded = () => ({
  type: 'TOGGLE_SAMPLE_LOCATION_EXPANDED'
});

export const toggleSamplePhotosExpanded = () => ({
  type: 'TOGGLE_SAMPLE_PHOTOS_EXPANDED'
});

export const toggleSampleNotesExpanded = () => ({
  type: 'TOGGLE_SAMPLE_NOTES_EXPANDED'
});

export const setSuccessMessage = text => ({
  type: 'SET_SUCCESS_MESSAGE',
  payload: text
});

export const clearSuccessMessage = () => ({
  type: 'CLEAR_SUCCESS_MESSAGE'
});
