const initialState = {
  sampleView: "main",

  sampleData: {
    title: "",
    code: "",
    datetime: "",
    date: "",
    time: "",
    location: {lat: null, lng: null},
    photosAmount: 0,
    notesText: "",
  },

  expanded: {
    codeExpanded: true,
    dateExpanded: true,
    locationExpanded: true,
    photosExpanded: true,
    notesExpanded: true,
  },

  successMessage: {
    show: false,
    text: "",
  }
};

export default ( state = initialState, { type, payload }) => {
  switch ( type ) {

    case 'CHANGE_SAMPLE_VIEW':
      return {
        ...state,
        sampleView: payload
      };

    case 'CHANGE_SAMPLE_TITLE':
      return {
        ...state,
        sampleData: {
          ...state.sampleData,
          title: payload,
        }
      }

    case 'CHANGE_SAMPLE_CODE':
      return {
        ...state,
        sampleData: {
          ...state.sampleData,
          code: payload,
        }
      }

    case 'CHANGE_SAMPLE_DATETIME':
      return {
        ...state,
        sampleData: {
          ...state.sampleData,
          datetime: payload,
        }
      }

    case 'CHANGE_SAMPLE_LOCATION':
      return {
        ...state,
        sampleData: {
          ...state.sampleData,
          location: payload,
        }
      }

    case 'CHANGE_SAMPLE_PHOTOS_AMOUNT':
      return {
        ...state,
        sampleData: {
          ...state.sampleData,
          photosAmount: payload,
        }
      }

    case 'CHANGE_SAMPLE_NOTES_TEXT':
      return {
        ...state,
        sampleData: {
          ...state.sampleData,
          notesText: payload,
        }
      }

    case 'TOGGLE_SAMPLE_CODE_EXPANDED':
      return {
        ...state,
        expanded: {
          ...state.expanded,
          codeExpanded: !state.expanded.codeExpanded,
        }
      }

    case 'TOGGLE_SAMPLE_DATETIME_EXPANDED':
      return {
        ...state,
        expanded: {
          ...state.expanded,
          dateExpanded: !state.expanded.dateExpanded,
        }
      }

    case 'TOGGLE_SAMPLE_LOCATION_EXPANDED':
      return {
        ...state,
        expanded: {
          ...state.expanded,
          locationExpanded: !state.expanded.locationExpanded,
        }
      }

    case 'TOGGLE_SAMPLE_PHOTOS_EXPANDED':
      return {
        ...state,
        expanded: {
          ...state.expanded,
          photosExpanded: !state.expanded.photosExpanded,
        }
      }

    case 'TOGGLE_SAMPLE_NOTES_EXPANDED':
      return {
        ...state,
        expanded: {
          ...state.expanded,
          notesExpanded: !state.expanded.notesExpanded,
        }
      }

    case 'SET_SUCCESS_MESSAGE':
      return {
        ...state,
        successMessage: {
          show: true,
          text: payload,
        }
      }

    case 'CLEAR_SUCCESS_MESSAGE':
      return {
        ...state,
        successMessage: {
          show: false,
          text: "",
        }
      }

    default:
      return state;
  }
};
