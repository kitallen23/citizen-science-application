export const changeSkipWelcome = value => ({
  type: 'CHANGE_SKIP_WELCOME',
  payload: value
});

export const changeDefaultScreen = screen => ({
  type: 'CHANGE_DEFAULT_SCREEN',
  payload: screen
});

export const changeTimeFormat = timeFormat => ({
  type: 'CHANGE_TIME_FORMAT',
  payload: timeFormat
});
