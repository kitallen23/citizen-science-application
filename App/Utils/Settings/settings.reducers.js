const initialState = {
  skipWelcome: false,
  defaultScreen: "Map",
  timeFormat: "12h",
};

export default ( state = initialState, { type, payload }) => {
  switch ( type ) {
    case 'CHANGE_SKIP_WELCOME':
      return {
        ...state,
        skipWelcome: payload,
      }
    case 'CHANGE_DEFAULT_SCREEN':
      return {
        ...state,
        defaultScreen: payload,
      }
    case 'CHANGE_TIME_FORMAT':
      return {
        ...state,
        timeFormat: payload,
      }
    default:
      return state;
  }
};
