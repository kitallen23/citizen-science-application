export const changeContent = screen => ({
  type: 'CHANGE_CONTENT',
  payload: screen
});

export const changeHome = screen => ({
  type: 'CHANGE_HOME',
  payload: screen
});

export const setDrawer = status => ({
  type: 'SET_DRAWER',
  payload: status
});

export const toggleDrawer = () => ({
  type: 'TOGGLE_DRAWER'
});
