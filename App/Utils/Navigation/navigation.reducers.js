const initialState = {
  content: "MySamples",
  home: "Map",
};

export default ( state = initialState, { type, payload }) => {
  switch ( type ) {

    case 'CHANGE_HOME':
      return {
        ...state,
        home: payload
      };

    case 'CHANGE_CONTENT':
      return {
        ...state,
        content: payload
      };

    case 'SET_DRAWER':
      return {
        ...state,
        drawer: payload
      };

    case 'TOGGLE_DRAWER':
      return {
        ...state,
        drawer: !state.drawer
      };

    default:
      return state;
  }
};
