import { combineReducers } from 'redux';

import navigation from '../Utils/Navigation/navigation.reducers.js';

export default combineReducers({
	navigation
});
