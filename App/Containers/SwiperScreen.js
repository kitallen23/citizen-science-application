import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TextInput, TouchableOpacity } from 'react-native'
import RoundedButton from '../Components/RoundedButton.js'

import { Images } from '../Themes'
import Swiper from 'react-native-swiper';

// Styles
import styles from './Styles/SwiperScreenStyles'

export default class SwiperScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: this.props.navigation.state.params.name,
      email: this.props.navigation.state.params.email,
      password: this.props.navigation.state.params.password
    };
  }

  static navigationOptions = {
    title: 'Introduction',
  }

  render () {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.mainContainer}>

        <Swiper style={styles.wrapper} showsButtons={false} dotColor="rgba(255,255,255,0.5)" activeDotColor="white" loop={false}>

          <View style={styles.slide1}>
            <Text style={styles.slideText}>Welcome, {this.state.name}!</Text>
            <Text style={styles.slideTextSmall}>It's time to start exploring, but not before some important instructions and tips.</Text>
          </View>

          <View style={styles.slide2}>
            <Text style={styles.slideText}>Request a kit</Text>
            <Text style={styles.slideTextSmall}>Order sampling kits from the main page, by tapping on the
              'Request Kit' button and filling out the following form. Easy!</Text>
            <Image source={Images.requestKit} style={styles.image} />
          </View>

          <View style={styles.slide3}>
            <Text style={styles.slideText}>Begin sampling</Text>
            <Text style={styles.slideTextSmall}> Find a water source, and start sampling by following the step-by-step instructions provided!</Text>
            <Image source={Images.beginSampling} style={styles.image} />
          </View>

          <View style={styles.slide4}>
            <Text style={styles.slideText}>Log your sample data</Text>
            <Text style={styles.slideTextSmall}>Store data from your samples into the app, and view them again with ease via the app, or at our web portal citizenscience.com!</Text>
            <Text style={styles.slideTextSmall}>* Insert image here *</Text>
          </View>

          <View style={styles.slide3}>
            <Text style={styles.slideText}>Are you ready?</Text>
            <TouchableOpacity style={styles.beginButton} onPress={() => navigate('MainScreen')}>
              <View style={styles.buttonContentContainer}>
                <Text style={styles.buttonText}>Let's begin!</Text>
              </View>
            </TouchableOpacity>

          </View>

        </Swiper>

      </View>
    )
  }
}
