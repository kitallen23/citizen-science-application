import React, { Component } from 'react'
import { ActivityIndicator, AsyncStorage, Keyboard, KeyboardAvoidingView, ScrollView, Text, Image, View, TextInput, TouchableOpacity, Picker } from 'react-native'
import TextInputLogin from '../Components/TextInputLogin.js'
import TextInputRequest from '../Components/TextInputRequest.js'
import RegisterButton from '../Components/RegisterButton.js'
import Icon from 'react-native-vector-icons/Ionicons';

import { Images, Colors } from '../Themes'
import Swiper from 'react-native-swiper';

// Styles
import styles from './Styles/RegistrationScreenStyles'

export default class RegistrationScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      streetAddress: '',
      city: '',
      postcode: '',
      state: 'ACT',
    };
    this.navigate = this.props.navigation.navigate
  }

  static navigationOptions = {
    title: 'Register',
  }

  submitRegistration(callback) {
    fetch('https://citsciapp.herokuapp.com/register', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
        name: this.state.name,
        streetAddress: this.state.streetAddress,
        city: this.state.city,
        postcode: this.state.postcode,
        state: this.state.state,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {

        if (responseJson.status == "SUCCESS") {
          AsyncStorage.setItem("token", responseJson.token).then(() => {
          AsyncStorage.setItem("refresh_token", responseJson.refresh_token).then(() => {
          AsyncStorage.setItem("account:name", this.state.name).then(() => {
          AsyncStorage.setItem("account:email", this.state.email).then(() => {
          AsyncStorage.setItem("account:streetAddress", this.state.streetAddress).then(() => {
          AsyncStorage.setItem("account:city", this.state.city).then(() => {
          AsyncStorage.setItem("account:postcode", this.state.postcode).then(() => {
          AsyncStorage.setItem("account:state", this.state.state).then(() => {
            callback();
          });
          });
          });
          });
          });
          });
          });
          });
        }
        else {
          this.refs.swiper.scrollBy(-2);
          this.setState({registerResponse: "Invalid information submitted"})
        }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  register() {
    Keyboard.dismiss();

    // Only if everything went well:
    this.refs.swiper.scrollBy(1);

    // Save to database, ensure everything went to plan
    this.submitRegistration(this.completeRegistration.bind(this));
  }

  completeRegistration() {
    this.navigate('SwiperScreen', {
      name: this.state.name,
      email: this.state.email,
    });
  }

  nextSlide() {
    Keyboard.dismiss();
    // Remove the registerResponse text
    this.setState({registerResponse: undefined});

    // Check that form data is correct here, before moving on

    // Only if correct:
    this.refs.swiper.scrollBy(1);
  }

  displayResponse() {
    return (
      this.state.registerResponse?
      <Text style={styles.responseText}>{this.state.registerResponse}</Text>
      :
      null
    );
  }

  render () {

    return (
      <View style={styles.mainContainer}>

        <Swiper style={styles.wrapper} ref="swiper" showsButtons={false} showsPagination={false} loop={false} scrollEnabled={false}>

          {/*** Register form slide ***/}
          <View style={styles.slide1}>

            <Image source={Images.citloginBg} style={styles.background} />

            <KeyboardAvoidingView behavior="padding">

              <View style={styles.centeredHorizontal}>
                <Image source={Images.citscilogo} style={styles.logo} />
              </View>

              { this.displayResponse() }

              <TextInputLogin
                keyboardType="default"
                placeholder="Full Name"
                onChangeText={(name) => this.setState({name})}
                value={this.state.name}
                returnKeyType="next"
                autoCapitalize="sentences"

                onSubmitEditing={(event) => {
                  this.refs.EmailInput.focus();
                  }}

                />

              <TextInputLogin
                keyboardType="email-address"
                placeholder="Email Address"
                onChangeText={(email) => this.setState({email})}
                value={this.state.email}
                returnKeyType="next"

                ref='EmailInput'

                onSubmitEditing={(event) => {
                  this.refs.PasswordInput.focus();
                  }}

                />

              <TextInputLogin
                placeholder="Password"
                onChangeText={(password) => this.setState({password})}
                value={this.state.password}
                returnKeyType="go"

                ref='PasswordInput'

                onSubmitEditing={() => {
                  this.refs.swiper.scrollBy(1)
                  this.refs.StreetAddress.focus();
                }}
                secureTextEntry

                />

              <TouchableOpacity
                onPress={() => this.nextSlide() }
                color="#00b7a8"
                style={styles.iconButton}>

                {/* <Text style={styles.buttonText}>Next</Text> */}
                <View style={styles.buttonContentContainer}>
                    <Text style={styles.buttonText}>NEXT </Text>
                    <Icon name="ios-arrow-forward" style={styles.icon} size={20} color={Colors.snow} />
                </View>
              </TouchableOpacity>

              <Text
                style={styles.linkTextSmall}
                onPress={() => this.navigate('LaunchScreen')}>
                Already have an account? Login here!
              </Text>

            </KeyboardAvoidingView>

          </View>

          {/*** Address form ***/}
          <View style={styles.slide1}>

            <KeyboardAvoidingView behavior="padding">
              <View style={styles.topBar}>
                <View style={styles.menuButtonContainer}>
                  <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.refs.swiper.scrollBy(-1) }>
                  <Text style={styles.centered}> <Icon name="ios-arrow-back-outline" size={35} color={Colors.snow} /> </Text>
                  </TouchableOpacity>
                </View>

              </View>

              <Image source={Images.citloginBg} style={styles.background} />


              <View style={styles.centeredHorizontal}>
                <Image source={Images.citscilogo} style={styles.logo} />
              </View>

              {/*** Street Address ***/}
              <TextInputLogin
                keyboardType="default"
                placeholder="Street Address"
                onChangeText={(streetAddress) => this.setState({streetAddress: streetAddress})}
                value={this.state.streetAddress}
                returnKeyType="next"

                ref='StreetAddress'

                onSubmitEditing={(event) => {
                  this.refs.City.focus();
                }}

              />

              {/*** City ***/}
              <TextInputLogin
                keyboardType="default"
                placeholder="City"
                onChangeText={(city) => this.setState({city: city})}
                value={this.state.city}
                returnKeyType="next"

                ref='City'

                onSubmitEditing={(event) => {
                  this.refs.Postcode.focus();
                }}

              />

              <View style={styles.formRow}>

                {/*** Postcode ***/}
                <View style={styles.formRowElement}>
                  <TextInput style={styles.textInput}
                    keyboardType="numeric"
                    placeholder="Postcode"
                    underlineColorAndroid={'rgba(0,0,0,0)'}
                      placeholderTextColor={Colors.secondaryLightColor}
                    onChangeText={(postcode) => this.setState({postcode: postcode})}
                    value={this.state.postcode}
                    returnKeyType="next"

                    ref='Postcode'

                    onSubmitEditing={(event) => {
                      Keyboard.dismiss();
                    }}

                  />
                </View>

                {/*** State ***/}
                <View style={styles.formRowElement}>
                  <View style={styles.statePickerWrapper}>
                    <Picker style={styles.statePicker}
                      selectedValue={this.state.state}
                      onValueChange={(value) => this.setState({state: value})}
                      ref='State'>

                      <Picker.Item label="ACT" value="ACT" />
                      <Picker.Item label="NSW" value="NSW" />
                      <Picker.Item label="NT" value="NT" />
                      <Picker.Item label="QLD" value="QLD" />
                      <Picker.Item label="SA" value="SA" />
                      <Picker.Item label="TAS" value="TAS" />
                      <Picker.Item label="VIC" value="VIC" />
                      <Picker.Item label="WA" value="WA" />
                    </Picker>
                  </View>
                </View>

              </View>

              <RegisterButton
                onPress={() => this.register()}

                ref='RegisterButton'
                />
            </KeyboardAvoidingView>

            <Text style={styles.linkTextSmall}>
              Note: Postal address is required for requesting sample kits
            </Text>
          </View>

          {/*** Creating account slide ***/}
          <View style={styles.slide1}>
            <View style={styles.spinnerWrapper}>
              <ActivityIndicator
                size="large"
                color="#fff"
                style={styles.spinner}
              />
            </View>

            <View style={styles.loginTextWrapper}>
              <Text style={styles.centerTextLarge}>Creating account...</Text>
            </View>
          </View>

        </Swiper>

      </View>
    )
  }
}
