import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text, View, Image } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import PageTitle from '../../../Components/PageTitle.js';

// Styles
import styles from './LegalScreenStyles'
import { Colors, Images } from '../../../Themes/'

export default class LegalScreen extends Component {

  static propTypes = {
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  goHome() {
    this.props.goHome();
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }
  changeContent(screen){
    this.props.changeContent(screen);
  }

  render () {

    return (

      <View style={styles.mainContainer}>

        <View style={styles.topBarWithTitle}>
          <View style={styles.menuButtonContainer}>
            <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.showDrawer() }>
            <Text style={styles.centered}> <Icon name="ios-menu" size={35} color={Colors.snow} /> </Text>
            </TouchableOpacity>
          </View>

          <PageTitle
            title="Legal"
          />

        </View>

      </View>

    )
  }
}
