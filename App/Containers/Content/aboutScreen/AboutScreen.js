import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text, View, Image } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import PageTitle from '../../../Components/PageTitle.js';

// Styles
import styles from './AboutScreenStyles'
import { Colors, Images } from '../../../Themes/'

export default class AboutScreen extends Component {

  static propTypes = {
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
  }


  constructor(props) {
    super(props);
    this.state = {
    };
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }

  render () {

    return (

      <View style={styles.mainContainer}>

        <View style={styles.topBarWithTitle}>
          <View style={styles.menuButtonContainer}>
            <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.showDrawer() }>
            <Text style={styles.centered}> <Icon name="ios-menu" size={35} color={Colors.snow} /> </Text>
            </TouchableOpacity>
          </View>

          <PageTitle
            title="About"
          />

        </View>

      </View>

    )
  }
}
