import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bold: {
    fontWeight: 'bold'
  },
  noSamples: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 22,
  },
  verticaList: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: 15,
  },
  listItem: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  image: {
    width: 80,
    height: 80,
    resizeMode: "cover",
    borderRadius: 100,
  },
  listContentContainer: {
    paddingTop: Metrics.topBarHeight,
    paddingHorizontal: Metrics.spacing.xl,
  },
  searchContainer: {
    margin: 5,
    flex: 1,
  },
  search: {
    borderRadius: 5,
    color: 'black',
    padding: 10,
    fontSize: 15,
    backgroundColor: Colors.snow,
    width: "100%",
    height: 50,
    elevation: 3,
  },
  searchResult: {
    fontSize: 18,
    color: "black",
    paddingVertical: 10,
  },
  resultsContainer: {
    position: "absolute",
    top: 70,
    right: 0,
    left: 0,
    paddingHorizontal: 10,
    marginHorizontal: Metrics.marginHorizontal,
    zIndex: 2,
    borderRadius: 5,
    backgroundColor: Colors.snow,
    elevation: 3,
    bottom: 10,
  },

  mainButtonsContainer: {
    position: "absolute",
    bottom: 30,
    right: 0,
    left: 0,
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row',
    zIndex: 1,
  },
  button: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    margin: 10,
    backgroundColor: Colors.buttonBackground,
    borderRadius: 3,
    elevation: 1,
    width: "40%",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontWeight: 'bold',
    color: Colors.snow,
    fontSize: Fonts.size.normal,
    textAlign: 'center',
  },

  slideMap: {
    flex: 1,
    justifyContent: 'center',
    width: "100%"
  },
  slideList: {
    flex: 1,
    justifyContent: 'center',
    width: "100%"
  },
})
