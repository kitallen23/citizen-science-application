import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ActivityIndicator, AsyncStorage, BackHandler, ScrollView, Keyboard, ListView, Alert, TouchableOpacity, Text, Image, View, TextInput } from 'react-native'
import RoundedButton from '../../../Components/RoundedButton.js'
import SampleMap from '../../../Components/SampleMap.js'
import Icon from 'react-native-vector-icons/Ionicons';
import Geocoder from 'react-native-geocoding';
import Swiper from 'react-native-swiper';
import PageTitle from '../../../Components/PageTitle.js';

// Styles
import { Colors, Images } from '../../../Themes/'
import styles from './ListScreenStyles'

APIKey = "AIzaSyAAIww5AeJtDGgbAiWzcfKmX_WZ0gA75GU"
Geocoder.setApiKey(APIKey);

export default class ListScreen extends Component {

  static propTypes = {
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
  }

  constructor(props) {
    super(props);


    this.state = {
      searchValue: "",
      searchResults: [],
      ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      region: {
        latitude: -37.814263,
        longitude: 144.961014,
        latitudeDelta: 0.2,
        longitudeDelta: 0.2,
      },
      samples: [],
      loading: true,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress.bind(this));
    this.loadSamples()

  }

  handleBackPress() {
    if (this.state.searchResults.length > 0) {
      this.setState({searchValue: "", searchResults: []})
    }
    else {
      BackHandler.exitApp();
    }
    return true;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onRegionChange(region) {
    this.setState({ region: region });
  }

  getMarkers(data) {
    samples = []
    data.forEach(function(sample,index) {
      if (sample.photo == "") {
        uri = null
      } else {
        uri = sample.photo
      }
      var date = new Date(sample.datetime);
      samples[index] = {
        "id": sample.id,
        "weather": sample.weather,
        "lat": parseFloat(sample.lat),
        "lng": parseFloat(sample.lng),
        "image": {uri: uri},
        "datetime": date.toLocaleString(),
      }
      this.getLocationName(sample.lat, sample.lng, index)
    }.bind(this) )
    this.setState({ samples: samples })
    this.setState({ loading: false });
  }

  loadSamples() {
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.getSamples(token, refresh_token);
      });
    });
  }
  getSamples(token, refresh_token) {
    fetch('https://citsciapp.herokuapp.com/samples', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // Do login token shit
        token: token,
        refresh_token: refresh_token,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {

      if (responseJson.status == "SUCCESS") {
        AsyncStorage.setItem("token", responseJson.token);
        AsyncStorage.setItem("refresh_token", responseJson.refresh_token);
        this.getMarkers(responseJson.data);
      }
      else {
        this.setState({loading: false});
        Alert.alert(
          "Error when retrieving samples",
          responseJson.message,
          [
            {text: 'Log in', onPress: () => this.logout() },
          ],
        );
      }
      this.setState({loading: false});
      return;
    })
    .catch((error) => {
      this.setState({loading: false});
      Alert.alert(
        "Error when retrieving samples",
        "Could not receive response from server, please check your internet connection and try again.",
        [
          {text: 'Log in', onPress: () => this.logout() },
        ],
      );
      console.error(error);
    });
  }

  logout() {
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.setState({loading: true});

        fetch('https://citsciapp.herokuapp.com/logout', {
          method: 'POST',
          headers:  {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            token: token,
            refresh_token: refresh_token,
          })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({loading: false});
        })
        .catch((error) => {
          console.error(error);
        });
      });
    });
    AsyncStorage.removeItem("token").then(() => {
      AsyncStorage.removeItem("refresh_token").then(() => {
        // Navigate to login screen
        this.props.goLogin();
        console.log("Logged out");
      });
    });
  }

  getLocationName(lat,long,index) {
    var latlng = `${lat},${long}`

    fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${APIKey}`)
      .then( (res) => res.json() )
      .then( (json) => {
        if (json.status == 'OK') {
          samples = this.state.samples
          samples[index].formatted_address = json.results[0].formatted_address
          this.setState({samples: samples})
        }
      });
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }
  changeContent(screen){
    this.props.changeContent(screen);
  }
  setMapLocation(location){
    this.props.setLocation(location)
  }

  //use this to load a place on the map
  getSampleLocation(location) {

    // location = {lat: 1234.123, lng: 1234.12}
    setMapLocation(location)
    changeContent("Map")
  }

  render () {

      const {samples} = this.state

    return (

        <View style={styles.mainContainer}>

          {/*** LIST VIEW ***/}
          <View style={styles.topBarWithTitle}>
            <View style={styles.menuButtonContainer}>
              <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.showDrawer() }>
              <Text style={styles.centered}> <Icon name="ios-menu" size={35} color={Colors.snow} /> </Text>
              </TouchableOpacity>
            </View>

            <PageTitle
              title="My Samples"
            />

            <View style={styles.menuButtonContainer}>
              <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.changeContent("Map") }>
              <Text style={styles.centered}> <Icon name="ios-map-outline" size={35} color={Colors.snow} /> </Text>
              </TouchableOpacity>
            </View>

          </View>

            {(this.state.loading) ?
              <View style={styles.empty}>
                <ActivityIndicator size={30} color={Colors.primaryColor}/>
              </View>

            : (

              <View>
                {(samples.length == 0) ? (
                  <View style={styles.empty}>
                    <Text style={styles.noSamples}>You have no samples</Text>
                  </View>
                ) : (
                  <ScrollView style={styles.listContentContainer}>
                    {samples.map((sample, i) =>
                      <View style={styles.listItem} key={i}>
                        <Image
                          style={styles.image}
                          source={sample.image}
                          key={sample.image}
                          />
                        <View style={styles.verticaList}>
                          <Text style={styles.bold}>Sample #{sample.id}</Text>
                          <Text>{sample.formatted_address}</Text>
                          <Text>{sample.datetime}</Text>
                        </View>
                      </View>
                    )}
                  </ScrollView>
                )}
                </View>


            )}

        </View>

    )
  }
}
