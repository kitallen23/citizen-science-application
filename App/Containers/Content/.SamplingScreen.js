import React, { Component } from 'react'
import { Keyboard, TextInput, Alert, Button, BackHandler, TouchableOpacity, Text, View, Image } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import DatePicker from 'react-native-datepicker'
import moment from "moment"

import Swiper from 'react-native-swiper';
import QRCodeScanner from '../../Components/QRCodeScanner.js';
import PageTitle from '../../Components/PageTitle.js';
import ImagePicker from 'react-native-image-picker';

// Styles
import styles from './Styles/SamplingScreenStyles'
import { Colors, Images, Metrics, Fonts } from '../../Themes/'

const dateInputStyle = {
  flex: 1,
  height: 40,
  borderWidth: 0,
  borderColor: '#fff',
  alignItems: 'center',
  justifyContent: 'center',
}
const dateTouchBodyStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: Colors.buttonBackground,
  borderRadius: 6,
  height: 50,
}
const dateTextStyle= {
  color: "white", fontSize: Fonts.size.h6, fontWeight: "bold"
}

var options = {
  title: 'Select Avatar',
  customButtons: [
    {name: 'fb', title: 'Choose Photo from Facebook'},
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

/**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info below in README)
 */

let minDate = "15 August 2016";

var day = new Date();
var dayWrapper = moment(day);
var maxDate = dayWrapper.format("D MMMM YYYY");

export default class SamplingScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      code: "",
      title: "",
      date: "",
      location: {lat: null, lng: null},
      notes: "",
      photosAmount: "",
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.goBack.bind(this));
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.goBack.bind(this));
  }

  imagePicker(){
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  showDrawer(){
    this.props.showDrawer();
  }
  closeDrawer(){
    this.props.closeDrawer();
  }
  changeContent(screen){
    this.props.changeContent(screen);
  }
  onScan(e) {
   this.setState({code: e.data});
  }
  scrollBack() {
    Keyboard.dismiss();
    this.refs.swiper.scrollBy(-1)
  }
  scrollNext() {
    Keyboard.dismiss();
    this.refs.swiper.scrollBy(1)
  }
  goBack() {
    this.changeContent("Map");
  }

  render () {

    return (

      <View style={styles.mainContainer}>

        <View style={styles.topBarWithTitle}>
          <View style={styles.menuButtonContainer}>
            <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.goBack() }>
            <Text style={styles.centered}> <Icon name="ios-arrow-back-outline" size={35} color={Colors.snow} /> </Text>
            </TouchableOpacity>
          </View>

          <PageTitle
            title="Record sample"
          />

        </View>

        <Swiper ref='swiper' style={styles.wrapper} showsButtons={false} dotColor="rgba(255,255,255,0.5)" activeDotColor="white" loop={false} scrollEnabled={false} index={8}>

          {/* Checklist */}
          <View style={styles.slideInfo}>
            <Text style={styles.slideInfoText}>Preparation</Text>
            <Text style={styles.slideTextSmall}>Please make sure to prepare the following materials before you proceed with your sampling. </Text>

            <View style={styles.textWrapper}>
              <View style={styles.listItem}>
                <Icon name="ios-checkmark-circle-outline" size={25} color="#fff"/>
                <Text style={styles.slideInfoTextSmallLeft}>There is one thing</Text>
              </View>

              <View style={styles.listItem}>
                <Icon name="ios-checkmark-circle-outline" size={25} color="#fff" />
                <Text style={styles.slideInfoTextSmallLeft}>Then another thing</Text>
              </View>

              <View style={styles.listItem}>
                <Icon name="ios-checkmark-circle-outline" size={25} color="#fff" />
                <Text style={styles.slideInfoTextSmallLeft}>Also this one thing</Text>
              </View>

              <View style={styles.listItem}>
                <Icon name="ios-checkmark-circle-outline" size={25} color="#fff" />
                <Text style={styles.slideInfoTextSmallLeft}>With a bit of this thing</Text>
              </View>

              <View style={styles.listItem}>
                <Icon name="ios-checkmark-circle-outline" size={25} color="#fff" />
                <Text style={styles.slideInfoTextSmallLeft}>And also another one</Text>
              </View>
            </View>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </View>

          </View>

          {/* prep kit  */}
          <View style={styles.slideInfo}>
            <Text style={styles.slideInfoText}>Step 1</Text>
            <Text style={styles.slideTextSmall}>Do this and this and this.</Text>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </View>

          </View>

          {/* obtain example */}
          <View style={styles.slideInfo}>
            <Text style={styles.slideInfoText}>Step 2</Text>
            <Text style={styles.slideTextSmall}>Do this and this and this.</Text>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </View>

          </View>

          {/* how to prep */}
          <View style={styles.slideInfo}>
            <Text style={styles.slideInfoText}>Step 3</Text>
            <Text style={styles.slideTextSmall}>Do this and this and this.</Text>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </View>

          </View>

          {/* scan qr page */}
          <View style={styles.slideNormal}>
            <Text style={styles.slideText}>Scan QR code</Text>
            <Text style={styles.slideTextSmall}>Scan the QR code attached to your kit</Text>
            <QRCodeScanner
              onRead={this.onScan.bind(this)}
              />
            <Text style={styles.slideTextSmall}>{this.state.code}</Text>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>

              {/* ENABLE AFTER TESTING

              {this.state.code ?
                <TouchableOpacity
                  onPress={() => this.scrollNext()}
                  color="#00b7a8"
                  style={styles.button}>

                  <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
              : null}

              */}

            </View>

          </View>

          {/* title */}
          <View style={styles.slideNormal}>
            <Text style={styles.slideText}>Title</Text>
            <Text style={styles.slideTextSmall}>Please follow this format: AAA-DD-Title-Year</Text>

            <TextInput
              style={styles.textInput}
              onChangeText={(text) => this.setState({title: text})}
              value={this.state.text}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              autoCorrect={false}
            />

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>

              {this.state.title ?
                <TouchableOpacity
                  onPress={() => this.scrollNext()}
                  color="#00b7a8"
                  style={styles.button}>

                  <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
              : null}

            </View>

          </View>

          {/* date */}
          <View style={styles.slideNormal}>
            <Text style={styles.slideText}>Date/Time</Text>
            <Text style={styles.slideTextSmall}>When did you take this sample?</Text>
             <DatePicker
              customStyles={{
                dateInput: dateInputStyle,
                dateText: dateTextStyle,
                placeholderText: dateTextStyle,
                dateTouchBody: dateTouchBodyStyle,
              }}
              style={styles.datepickerStyle}
              date={this.state.date}
              mode="date"
              format="D MMMM YYYY"
              minDate={minDate}
              maxDate={maxDate}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              onDateChange={(date) => {this.setState({date: date})}}
            />

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>


              {this.state.date ?
                <TouchableOpacity
                  onPress={() => this.scrollNext()}
                  color="#00b7a8"
                  style={styles.button}>

                  <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
              : null}

            </View>

          </View>

          {/* location */}
          <View style={styles.slideNormal}>
            <Text style={styles.slideText}>Location</Text>
            <Text style={styles.slideTextSmall}>Where did you take this sample?</Text>
            <Text style={styles.slideTextSmall}>** Map with location picker **</Text>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>


              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>


              {/* ENABLE AFTER TESTING

              {this.state.location.lat && this.state.location.lng ?
                <TouchableOpacity
                  onPress={() => this.scrollNext()}
                  color="#00b7a8"
                  style={styles.button}>

                  <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
              : null}

              */}

            </View>

          </View>

          {/* photos */}
          <View style={styles.slideNormal}>
            <Text style={styles.slideText}>Photos</Text>
            <Text style={styles.slideTextSmall}>Please attach the photos of your sample</Text>

            <TouchableOpacity
              onPress={() => this.imagePicker()}
              color="#00b7a8"
              style={styles.button}>

              <Text style={styles.buttonText}>Select</Text>
            </TouchableOpacity>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>


              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>


              {/* ENABLE AFTER TESTING

              {this.state.photosAmount ?
                <TouchableOpacity
                  onPress={() => this.scrollNext()}
                  color="#00b7a8"
                  style={styles.button}>

                  <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
              : null}

              */}

            </View>

          </View>

          {/* notes */}
          <View style={styles.slideNormal}>
            <Text style={styles.slideText}>Notes</Text>
            <Text style={styles.slideTextSmall}>Any notes you would like to include?</Text>

            <TextInput
              style={styles.textInputMulti}
              onChangeText={(text) => this.setState({notes: text})}
              value={this.state.text}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              autoCorrect={false}
              multiline
            />

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </View>

          </View>

          {/* summary of info */}
          <View style={styles.slideInfo}>
            <Text style={styles.slideInfoText}>Summary</Text>

            <View style={styles.textWrapper}>
              <View style={styles.listItem}>
                <Text style={styles.slideInfoTextSmallLeft}>Code: </Text>
                <Text style={styles.slideInfoTextSmall}>Hello {this.state.code}</Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.slideInfoTextSmallLeft}>Title: </Text>
                <Text style={styles.slideInfoTextSmall}>Hello {this.state.title}</Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.slideInfoTextSmallLeft}>Date: </Text>
                <Text style={styles.slideInfoTextSmall}>Hello {this.state.date}</Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.slideInfoTextSmallLeft}>Location: </Text>
                <Text style={styles.slideInfoTextSmall}>Hello {this.state.location.lat}, {this.state.location.lng}</Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.slideInfoTextSmallLeft}>Photos: </Text>
                <Text style={styles.slideInfoTextSmall}>Hello {this.state.photosAmount}</Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.slideInfoTextSmallLeft}>Notes: </Text>
                <Text style={styles.slideInfoTextSmall}>Hellohkfj ahadshk hkdsaj,n ubndask saudabui{this.state.notes}</Text>
              </View>
            </View>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.scrollBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Back</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.scrollNext()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </View>

          </View>

          {/* thank you for submitting */}
          <View style={styles.slideInfo}>
            <Text style={styles.slideInfoText}>Thank You For Submitting!</Text>
            <Text style={styles.slideInfoTextSmall}>Don't forget to deliver your sample to our labs at RMIT!</Text>
            <Text style={styles.slideInfoTextSmallLeft}>** address **</Text>
            <Text style={styles.slideInfoTextSmallLeft}>** address **</Text>
            <Text style={styles.slideInfoTextSmallLeft}>** address **</Text>
            <Text style={styles.slideInfoTextSmallLeft}>** address **</Text>

            <View style={styles.swiperNav}>
              <TouchableOpacity
                onPress={() => this.goBack()}
                color="#00b7a8"
                style={styles.button}>

                <Text style={styles.buttonText}>Done</Text>
              </TouchableOpacity>

            </View>

          </View>

        </Swiper>


        {/*
        swiper screen:
          checklist ->
            next
          prep kit ->
            back or next
          obtain example ->
            back or next
          how to prep ->
            back or next
          scan qr page ->
            back or next
          title ->
            back or next
          date (dropdown or today) ->
            back or next
          location (select or gps) ->
            back or next
          photos (optional) ->
            back or next
          notes ->
            back or next
          summary of info (state) ->
            back or next
          thank you for submitting OR error based on API response ->
            home or new sample
        */}

      </View>

    )
  }
}
