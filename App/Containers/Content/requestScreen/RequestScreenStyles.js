import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  requestContentContainer: {
    // paddingHorizontal: Metrics.spacing.xl,
  },
  formContainer: {
    paddingLeft: 0,
    paddingRight: 0,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 40,
  },
  formRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  formRowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  largeText: {
    marginBottom: Metrics.spacing.large,
    fontSize: Fonts.size.normal,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  smallTextWrapper: {
    paddingVertical: Metrics.spacing.tiny,
  },
  smallText: {
    fontSize: Fonts.size.normal,
    fontWeight: 'bold',
    textAlign: 'left',
    color: Colors.onLightTextColor,
    marginVertical: Metrics.spacing.tiny,
  },
  smallSubtext: {
    fontSize: Fonts.size.small,
    textAlign: 'left',
    color: Colors.secondaryTextColor,
  },
  smallTextCenter: {
    fontSize: Fonts.size.normal,
    textAlign: 'center',
  },
  textInputRequest:
  {
    height: 45,
    marginVertical: Metrics.spacing.medium,
    fontSize: Fonts.size.normal,
  },
  kitSubContainer: {
    marginTop: 15,
  },
  kitsPickerWrapper: {
    borderColor: Colors.borderColorLight,
    // borderColor: Colors.transparent,
    borderWidth: 1,
    borderRadius: 5,
    height: 45,
    paddingLeft: 16,
  },
  kitsPicker: {
    padding: 0,
    paddingRight: -30,
    height: 40,
    width: 75,
  },
  submitRequestWrapper: {
    marginBottom: 10,
    marginTop: 10,
  },
  addressSubContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomColor: Colors.borderColorLight,
    borderBottomWidth: 1,
    paddingVertical: Metrics.spacing.large,
  },
  addressTextHeader: {
    fontSize: Fonts.size.large,
    fontWeight: 'bold',
    flex: 1,
    marginBottom: 10,
    color: Colors.onLightTextColor,
  },
  addressTextSmallLeft: {
    fontSize: 18,
    flex: 1,
    textAlign: 'left',
    paddingTop: 6,
    paddingBottom: 6,
  },

  textHeader: {
    fontSize: Fonts.size.large,
    fontWeight: 'bold',
    color: Colors.onLightTextColor,
  },

  editButton: {
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.transparent,
    marginBottom: Metrics.spacing.medium,
  },
  editButtonText: {
    color: Colors.snow,
    fontSize: Fonts.size.normal,
  },

  slide1: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.primaryColor,
    width: "100%",
    paddingHorizontal: Metrics.spacing.xl,
  },
  centerTextLarge: {
    color: '#fff',
    fontSize: Fonts.size.h4,
    textAlign: 'center'
  },
  centerText: {
    color: '#fff',
    fontSize: Fonts.size.input,
    textAlign: 'center'
  },
  spinner: {
    transform: [
      {scale: 1.5}
    ]
  },
  spinnerWrapper: {
    paddingBottom: 45,
  },
  responsePageWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  goHomeButton: {
    height: 45,
    borderRadius: 5,
    marginVertical: Metrics.baseMargin+20,
    backgroundColor: Colors.primaryDarkColor,
    justifyContent: 'center',
    borderColor: Colors.transparent,
    borderWidth: 2,
    padding: 20,
  },
})
