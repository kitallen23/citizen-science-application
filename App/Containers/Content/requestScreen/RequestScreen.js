import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Keyboard, KeyboardAvoidingView, BackHandler, TouchableOpacity, Text, View, Image, Picker, TextInput, Switch, AsyncStorage, ActivityIndicator, Alert } from 'react-native';
import Toast, { DURATION } from 'react-native-easy-toast';
import Icon from 'react-native-vector-icons/Ionicons';
import PageTitle from '../../../Components/PageTitle.js';
import TextInputRequest from '../../../Components/TextInputRequest.js'
import GenericButton from '../../../Components/GenericButton.js'
import LoadingTopBar from '../../../Components/LoadingTopBar.js';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Swiper from 'react-native-swiper';
import RoundedButton from '../../../Components/RoundedButton.js';
import EditPostalDialog from '../../../Components/EditPostalDialog';

// Styles
import styles from './RequestScreenStyles'
import { Colors, Images } from '../../../Themes/'

export default class RequestScreen extends Component {

 static propTypes = {
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      address: {
        streetAddress: "",
        city: "",
        postcode: "",
        state: "",
      },
      numKits: 1,
      loading: true,
      loadSuccess: false,
      token: '',
      refresh_token: '',
      requestTextTitle: '',
      requestText: '',
      showPostalDialog: false,
    };
  }

  getProfile(token, refresh_token) {
    this.setState({loading: true})
    fetch('https://citsciapp.herokuapp.com/profile', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: token,
        refresh_token: refresh_token,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log("Response:");
      console.log(responseJson);

      if (responseJson.status == "SUCCESS") {
        AsyncStorage.setItem("token", responseJson.token);
        AsyncStorage.setItem("refresh_token", responseJson.refresh_token);

        // Ensure our AsyncStorage is up to date
        if(this.state.address !== responseJson.address)
        {
          AsyncStorage.setItem("account:streetAddress", responseJson.address.streetAddress).then(() => {
          AsyncStorage.setItem("account:city", responseJson.address.city).then(() => {
          AsyncStorage.setItem("account:postcode", responseJson.address.postcode).then(() => {
          AsyncStorage.setItem("account:state", responseJson.address.state).then(() => {
          });
          });
          });
          });
        }
        if(this.state.name !== responseJson.name)
        {
          AsyncStorage.setItem("account:name", responseJson.name);
        }
        if(this.state.email !== responseJson.email)
        {
          AsyncStorage.setItem("account:name", responseJson.name);
        }
        this.setState({name: responseJson.name});
        this.setState({email: responseJson.email});
        this.setState({address: responseJson.address});
        this.setState({loadSuccess: true});
      }
      else {
        Alert.alert(
          "Error when retrieving profile",
          responseJson.message,
          [
            {text: 'Log in', onPress: () => this.logout() },
          ],
          { cancelable: false }
        );
      }
      this.setState({loading: false});
      return;
    })
    .catch((error) => {
      this.setState({loading: false});
      Alert.alert(
        "Error when retrieving profile",
        "Could not receive response from server, please check your internet connection and try again.",
        [
          {text: 'Log in', onPress: () => this.logout()},
        ],
        { cancelable: false }
      )
      console.error(error);
    });
  }

  logout() {
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.setState({loading: true});

        fetch('https://citsciapp.herokuapp.com/logout', {
          method: 'POST',
          headers:  {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            token: token,
            refresh_token: refresh_token,
          })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({loading: false});
        })
        .catch((error) => {
          console.error(error);
        });
      });
    });
    AsyncStorage.removeItem("token").then(() => {
      AsyncStorage.removeItem("refresh_token").then(() => {
        // Navigate to login screen
        this.props.goLogin();
        console.log("Logged out");
      });
    });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.goBack.bind(this));

    // load profile from local storage
    AsyncStorage.getItem("account:name").then((name) => {
    AsyncStorage.getItem("account:email").then((email) => {
    AsyncStorage.getItem("account:streetAddress").then((streetAddress) => {
    AsyncStorage.getItem("account:city").then((city) => {
    AsyncStorage.getItem("account:postcode").then((postcode) => {
    AsyncStorage.getItem("account:state").then((state) => {
      this.setState({name: name});
      this.setState({email: email});
      this.setState({
        address: {
          ...this.state.address,
          streetAddress: streetAddress,
          city: city,
          postcode: postcode,
          state: state,
        }
      });

      // load profile from api
      AsyncStorage.getItem("token").then((token) => {
        AsyncStorage.getItem("refresh_token").then((refresh_token) => {
          this.getProfile(token, refresh_token);
          this.setState({
            token: token,
            refresh_token: refresh_token,
          })
        });
      });

    });
    });
    });
    });
    });
    });

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.goBack.bind(this));
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }
  changeContent(screen){
    this.props.changeContent(screen);
  }
  goBack() {
    this.changeContent("Map");
  }

  requestKits() {
    this.refs.swiper.scrollBy(1);
    fetch('https://citsciapp.herokuapp.com/request', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        refresh_token: this.state.refresh_token,
        quantity: this.state.numKits,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.status == "SUCCESS") {
        console.log(responseJson.status);
        this.setState({
          requestTextTitle: "Success! Request placed.",
        });
      }
      else {
        this.setState({
          requestTextTitle: "Oops! Something went wrong.",
        });
      }
      this.setState({requestText: responseJson.message});
      AsyncStorage.setItem("token", responseJson.token);
      AsyncStorage.setItem("refresh_token", responseJson.refresh_token);

      this.refs.swiper.scrollBy(1);
      return;
    })
    .catch((error) => {
      console.error(error);
    });
  }

  closePostalDialog() {
    this.setState({showPostalDialog: false});
  }

  setPostalDetails(t_address) {
    this.setState({address: t_address});
    this.refs.editSuccess.show('Postal address updated!', 3000);
  }

  render () {

    return (

      <View style={styles.mainContainer}>

        <Toast
          ref="editSuccess"
          position='bottom'
          opacity={1}
          style={{backgroundColor: '#4BBF73'}}
        />

        <Swiper style={styles.wrapper} ref="swiper" showsButtons={false} showsPagination={false} loop={false} scrollEnabled={false}>
          <View>

            <View style={styles.topBarWithTitle}>
              <View style={styles.menuButtonContainer}>
                <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.goBack() }>
                <Text style={styles.centered}> <Icon name="ios-arrow-back-outline" size={35} color={Colors.snow} /> </Text>
                </TouchableOpacity>
              </View>

              <PageTitle
                title="Request Sample Kit"
              />

              { this.state.loading ?
                <LoadingTopBar />
                : null
              }

            </View>

            <View style={styles.contentContainer}>
              <View style={styles.requestContentContainer}>

                {this.state.loadSuccess ? (
                  <EditPostalDialog
                    show={this.state.showPostalDialog}
                    onDismiss={() => this.closePostalDialog()}
                    address={this.state.address}
                    onSuccess={this.setPostalDetails.bind(this)}
                  />
                ) : null }

                <Text style={styles.smallTextCenter}>
                  Request a sampling kit through the post.
                </Text>
                <Text style={styles.smallTextCenter}>
                  Please ensure your postal address below is correct.
                </Text>

                <View style={styles.addressSubContainer}>

                  <View style={styles.formRow}>
                    <Text style={styles.addressTextHeader}>Postal Address</Text>

                    {this.state.loadSuccess ? (
                      <TouchableOpacity style={styles.editButton} onPress={ () => this.setState({showPostalDialog: true}) }>
                        <MaterialIcon name="edit" size={20} color={Colors.primaryDarkColor} />
                      </TouchableOpacity>
                    ) : (
                      <View style={styles.editButton}>
                        <MaterialIcon name="edit" size={20} color={Colors.primaryDarkColorDisabled} />
                      </View>
                    )}
                  </View>

                  <View style={styles.formRow}>
                    <Text style={styles.addressTextSmallLeft}>{this.state.address.streetAddress}</Text>
                  </View>

                  <View style={styles.formRow}>
                      <Text style={styles.addressTextSmallLeft}>{this.state.address.city}</Text>
                  </View>

                  <View style={styles.formRow}>
                    <Text style={styles.addressTextSmallLeft}>{this.state.address.state} {this.state.address.postcode}</Text>
                  </View>

                </View>

                {/*** # of kits ***/}
                <View style={styles.formRow}>
                  <View style={styles.kitSubContainer}>
                    <Text style={styles.textHeader}>Quantity</Text>
                  </View>
                </View>
                <View style={styles.formRowCenter}>
                  {/* <View style={styles.smallTextWrapper}> */}
                    <Text style={styles.smallSubtext}>Number of sample kits you wish to order</Text>
                  {/* </View> */}

                  <View style={styles.kitsPickerWrapper}>
                    <Picker style={styles.kitsPicker}
                      selectedValue={this.state.numKits}
                      onValueChange={(value) => this.setState({numKits: value})}
                      ref='NumKits'>

                      <Picker.Item label="1" value="1" />
                      <Picker.Item label="2" value="2" />
                      <Picker.Item label="3" value="3" />
                      <Picker.Item label="4" value="4" />
                      <Picker.Item label="5" value="5" />
                    </Picker>
                  </View>
                </View>
              </View>

              {/*** Submit request ***/}
              <View style={styles.submitRequestWrapper}>
                <GenericButton
                  onPress={() => this.requestKits()}
                  text="Submit request"

                  ref='SubmitButton'
                />
              </View>
            </View>

          </View>

          {/* Loading screen etc. */}
          <View style={styles.slide1}>
            <View style={styles.responsePageWrapper}>

            <View style={styles.spinnerWrapper}>
              <ActivityIndicator
                size="large"
                color="#fff"
                style={styles.spinner}
              />
            </View>

              <Text style={styles.centerTextLarge}>Processing request...</Text>
            </View>
          </View>

          <View style={styles.slide1}>
            <View style={styles.responsePageWrapper}>
                <Text style={styles.centerTextLarge}>{ this.state.requestTextTitle }</Text>
                <View style={{marginTop:20}}>
                  <Text style={styles.centerText}>{ this.state.requestText }</Text>
                </View>

                  <RoundedButton
                    onPress={() => this.goBack()}
                    text={'Return home'}

                    style={styles.goHomeButton}
                  />
            </View>
          </View>

        </Swiper>

      </View>

    )
  }
}
