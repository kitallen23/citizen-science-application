import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RequestScreen from './RequestScreen.js';
import { setDrawer, changeContent } from '../../../Utils/Navigation/navigation.actions.js';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ setDrawer, changeContent }, dispatch );
};

export default connect( mapStateToProps, mapDispatchToProps )( RequestScreen );
