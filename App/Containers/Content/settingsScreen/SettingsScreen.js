import React, { Component } from 'react';
import { ActivityIndicator, Alert, TouchableOpacity, Text, View, Image, Switch, Picker, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import PageTitle from '../../../Components/PageTitle.js';
import LoadingTopBar from '../../../Components/LoadingTopBar.js';
import GenericButton from '../../../Components/GenericButton.js';
import PropTypes from 'prop-types';

// Styles
import styles from './SettingsScreenStyles';
import { Colors, Images, Metrics } from '../../../Themes/';

export default class SettingsScreen extends Component {

  static propTypes = {
    settings: PropTypes.object,
    changeSkipWelcome: PropTypes.func,
    changeDefaultScreen: PropTypes.func,
    changeTimeFormat: PropTypes.func,
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
        loading: false,
    };
  }

  goLogin() {
    this.props.goLogin();
  }

  showDrawer(){
    this.props.setDrawer(true);
  }

  logout(token, refresh_token, callback) {
    this.setState({loading: true});

    fetch('https://citsciapp.herokuapp.com/logout', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // Do login token shit
        token: token,
        refresh_token: refresh_token,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({loading: false});
      if (responseJson.status == "SUCCESS") {
        Alert.alert("See you later!", "You have successfully logged out.");
        AsyncStorage.removeItem("token").then(() => {
          AsyncStorage.removeItem("refresh_token").then(() => {
            callback();
          });
        });
      }
      else {
        Alert.alert("Oops!", "Error occured, you were not logged in.");
        AsyncStorage.removeItem("token").then(() => {
          AsyncStorage.removeItem("refresh_token").then(() => {
            callback();
          });
        });
      }

    })
    .catch((error) => {
      console.error(error);
    });
  }

  startLogout() {
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.logout(token, refresh_token, this.exit.bind(this));
      });
    });
  }

  exit() {
    console.log("Settings screen: Logging out");
    this.goLogin();
    this.props.changeContent("Map")
  }

  componentDidMount() {
    console.log(this.props.settings);
    // this.props.changeTimeFormat("24h");

    // Do this for each setting
    AsyncStorage.getItem("@Settings:skipWelcome")
    .then((value) => {
      if(value != undefined) {
        setting = JSON.parse(value)
        console.log(setting)
        this.props.changeSkipWelcome(setting)
      }
    });

    AsyncStorage.getItem("@Settings:defaultScreen")
    .then((value) => {
      if(value != undefined) {
        setting = value
        console.log(setting)
        this.props.changeDefaultScreen(setting)
      }
    });

    AsyncStorage.getItem("@Settings:timeFormat")
    .then((value) => {
      if(value != undefined) {
        setting = value
        console.log(setting)
        this.props.changeTimeFormat(setting)
      }
    });
  }

  saveSetting(key, value) {
    AsyncStorage.setItem(key, value);
    console.log("Setting saved: " + key + " = " + value);
  }

  handleSkipWelcomeChange(value) {
    this.props.changeSkipWelcome(value);
    this.saveSetting("@Settings:skipWelcome", (value? "true":"false"));
  }

  handleDefaultScreenChange(value) {
    this.props.changeDefaultScreen(value)
    this.saveSetting("@Settings:defaultScreen", value);
  }

  handleTimeFormatChange(value) {
    this.props.changeTimeFormat(value);
    this.saveSetting("@Settings:timeFormat", value);
  }


  render () {

    return (

      <View style={styles.mainContainer}>

        <View style={styles.topBarWithTitle}>
          <View style={styles.menuButtonContainer}>
            <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.showDrawer() }>
            <Text style={styles.centered}> <Icon name="ios-menu" size={35} color={Colors.snow} /> </Text>
            </TouchableOpacity>
          </View>

          <PageTitle
            title="Settings"
          />

          { this.state.loading ?
            <LoadingTopBar />
            : null
          }

        </View>

        <View style={styles.settingsContentContainer}>

          {/*** Skip welcome screen ***/}
          <View style={styles.settingsWrapper}>
            <Text style={styles.settingsText}>Skip welcome</Text>
            <View style={styles.settingsRow}>
              <Text style={styles.settingsSubtext}>Dismiss welcome messages upon login</Text>
              <Switch
                onValueChange={ (value) => this.handleSkipWelcomeChange(value) }
                value={this.props.settings.skipWelcome}
              />
            </View>
          </View>

          {/*** Default screen ***/}
          <View style={styles.settingsWrapperPicker}>
            <Text style={styles.settingsText}>Default screen</Text>
            <View style={styles.pickerSettingsRow}>
              <Text style={styles.settingsSubtext}>Set default view for your samples</Text>
              <Picker style={styles.viewPicker}
                selectedValue={this.props.settings.defaultScreen}
                onValueChange={(itemValue, itemIndex) => this.handleDefaultScreenChange(itemValue)}>
                <Picker.Item label="Map" value="Map" />
                <Picker.Item label="List" value="List" />
              </Picker>
            </View>
          </View>

          {/*** Time format ***/}
          <View style={styles.settingsWrapperPicker}>
            <Text style={styles.settingsText}>Time format</Text>
            <View style={styles.pickerSettingsRow}>
              <Text style={styles.settingsSubtext}>Set default time format</Text>
              <Picker style={styles.viewPicker}
                selectedValue={this.props.settings.timeFormat}
                onValueChange={(itemValue, itemIndex) => this.handleTimeFormatChange(itemValue)}>
                <Picker.Item label="12h" value="12h" />
                <Picker.Item label="24h" value="24h" />
              </Picker>
            </View>
          </View>

          {/*** Logout ***/}
          <View style={styles.settingsRowLast}>
            <TouchableOpacity style={styles.logoutButton} onPress={ () => this.startLogout() }>
              <View style={styles.buttonContentContainer}>
                  <Text style={styles.buttonText}>Logout </Text>
                  <Icon name="ios-log-out" style={styles.icon} size={20} color={Colors.snow} />
              </View>
            </TouchableOpacity>
          </View>

        </View>

      </View>

    )
  }
}
