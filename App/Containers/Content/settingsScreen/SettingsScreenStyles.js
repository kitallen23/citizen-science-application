import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Fonts, Colors } from '../../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  settingsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "space-between",
    marginVertical: Metrics.spacing.small,
  },
  settingsRowCenter: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "space-between",
  },
  settingsRowLast: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "center",
    paddingVertical: Metrics.spacing.xl,
  },
  background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    resizeMode: 'stretch',
  },
  settingsText: {
    fontSize: Fonts.size.normal,
    fontWeight: "bold",
    color: Colors.onLightTextColor,
    marginTop: 4,
  },
  settingsSubtext: {
    fontSize: Fonts.size.small,
    color: Colors.secondaryTextColor,
  },
  viewPicker: {
    width: 100,
  },

  viewPickerItem: {
    paddingHorizontal: Metrics.spacing.medium,
    backgroundColor: Colors.primaryColor,
  },
  settingsContentContainer: {
    paddingTop: Metrics.topBarHeight,
    paddingHorizontal: Metrics.spacing.xl,
  },
  pickerSettingsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "space-between",
  },
  settingsWrapper: {
    paddingTop: Metrics.spacing.medium,
    paddingBottom: Metrics.spacing.medium,
    borderBottomColor: Colors.borderColorLight,
    borderBottomWidth: 1,
  },
  settingsWrapperPicker: {
    paddingTop: Metrics.spacing.medium,
    paddingBottom: Metrics.spacing.small,
    borderBottomColor: Colors.borderColorLight,
    borderBottomWidth: 1,
  },
  logoutButton: {
    flex: 1,
    alignItems: 'center',
    padding: Metrics.spacing.medium,
    borderRadius: 5,
    backgroundColor: Colors.red,
    elevation: 1,

  },
  buttonContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonText: {
    color: Colors.snow,
    fontSize: Fonts.size.normal,
    fontWeight: "bold",
    marginHorizontal: Metrics.spacing.medium,
  },
})
