import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SettingsScreen from './SettingsScreen.js';
import { setDrawer, changeContent } from '../../../Utils/Navigation/navigation.actions.js';
import { changeSkipWelcome, changeDefaultScreen, changeTimeFormat } from '../../../Utils/Settings/settings.actions.js';

const mapStateToProps = state => ({
  settings: state.settings,
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ changeContent, setDrawer, changeSkipWelcome, changeDefaultScreen, changeTimeFormat }, dispatch );
};

export default connect( mapStateToProps, mapDispatchToProps )( SettingsScreen );
