import React, { Component } from 'react';
import { View, AsyncStorage, Alert } from 'react-native';
import PropTypes from 'prop-types';
import styles from '../../mainScreen/MainScreenStyles';
import moment from 'moment';

import SamplingSummaryScreen from './samplingSummaryScreen';
import KitCodeScreen from './kitCodeScreen';

export default class SamplingMainScreen extends Component {

  static propTypes = {
    sampleView: PropTypes.string,
    changeSampleView: PropTypes.func,

    // sampleData: PropTypes.object,
    // changeSampleTitle: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      notesTextTemp: "",

      buttonText: "0/6",
      timeFormat: "12h",
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("@Settings:timeFormat")
    .then((value) => {
      if(value != undefined) {
        setting = value
        this.setState({timeFormat: setting})
      }
    });
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }
  changeContent(screen){
    this.props.changeContent(screen);
  }

  render () {

    return (
      <View style={styles.mainContainer}>
        {(() => {
          switch (this.props.sampleView) {
            case 'main':
              return <SamplingSummaryScreen
                  timeFormat={this.state.timeFormat}
                />
            case 'code':
              return <KitCodeScreen />
            default :
              null
          }
        })()}
      </View>
    )
  }
}
