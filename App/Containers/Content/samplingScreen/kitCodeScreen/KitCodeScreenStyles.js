import { StyleSheet } from 'react-native';
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../../Themes/';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
  },
  centerContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {
    paddingTop: Metrics.topBarHeight,
    backgroundColor: Colors.primaryColor,
    flex: 1,
  },
  loadingContentContainer: {
    backgroundColor: Colors.primaryColor,
    flex: 1,
  },
  centerContentSpacing: {
    marginHorizontal: Metrics.spacing.xl,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide: {
    flex: 1,
    width: "100%",
  },
  largeButton: {
    width: '100%',
    height: '50%',
    borderColor: Colors.primaryColorBorder,
    borderBottomWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  largeButtonText: {
    color: Colors.onDarkTextColor,
    fontSize: Fonts.size.h3,
  },
  largeIcon: {
    marginBottom: Metrics.spacing.large,
  },

  descriptionText: {
    color: Colors.onDarkTextColor,
    fontSize: Fonts.size.large,
    marginBottom: 30,
    textAlign: 'center',
  },
  codeInput: {
      backgroundColor: Colors.snow,
      height: 45,
      borderColor: Colors.transparent,
      borderRadius: 5,
      borderWidth: 2,
      paddingRight: 20,
      paddingLeft: 20,
      marginHorizontal: Metrics.loginItems,
      fontSize: 15,
  },

  spinner: {
    transform: [
      {scale: 1.5}
    ]
  },
  spinnerWrapper: {
    paddingBottom: 45,
  },
  centerTextLarge: {
    color: '#fff',
    fontSize: Fonts.size.h4,
    textAlign: 'center',
  },

  submitCodeWrapper: {
    marginBottom: 10,
    marginTop: 10,
    // marginHorizontal: 50,
  },
  submitButton: {
    height: 45,
    borderRadius: 5,
    marginVertical: Metrics.baseMargin,
    backgroundColor: Colors.primaryDarkColor,
    justifyContent: 'center',
    borderColor: Colors.transparent,
    borderWidth: 2,
    padding: 20,
    flex: 1,
    marginHorizontal: 50,
  },
  submitButtonText: {
    color: Colors.snow,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
  }
})
