import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import KitCodeScreen from './KitCodeScreen.js';
import { changeSampleView, changeSampleCode, setSuccessMessage } from '../../../../Utils/Sampling/sampling.actions.js';

const mapStateToProps = state => ({
  sampleCode: state.sampling.sampleData.code,
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
      changeSampleView,
      changeSampleCode,
      setSuccessMessage,
    }, dispatch );
};

export default connect( mapStateToProps, mapDispatchToProps )( KitCodeScreen );
