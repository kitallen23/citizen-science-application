import React, { Component } from 'react';
import { ScrollView, AsyncStorage, TouchableOpacity, Text, View, Image, TextInput, KeyboardAvoidingView, ActivityIndicator, Keyboard } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Swiper from 'react-native-swiper';
import MaterialCommIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Toast, { DURATION } from 'react-native-easy-toast';

import PageTitle from '../../../../Components/PageTitle.js';
import LoadingTopBar from '../../../../Components/LoadingTopBar.js';
import QRCodeScanner from '../../../../Components/QRCodeScanner.js';
import HelpModal from '../../../../Components/HelpModal.js';

// Styles
import styles from './KitCodeScreenStyles';
import { Colors, Images } from '../../../../Themes/';

// APIKey = "AIzaSyAAIww5AeJtDGgbAiWzcfKmX_WZ0gA75GU"

// TODO: Update this text when we know more info
let helpText = `The sample kit code is used so we can identify your sample. \
When we receive your sample in the mail, we must be able to link it to the \
data that you submit from this page -- the sample code lets us do just that!

There are two methods of attaching your sample kit code to your sampling \
process: scanning your QR code, or entering the code manually.

The QR code is located << ENTER STUFF HERE, NOT QUITE SURE WHERE THE QR CODE \
WILL GO >>. Select the "Scan QR Code" button below, then scan the QR code \
using your camera's phone.

Alternatively, you can enter the code manually. This code is located \
underneath the QR code on your sample kit.
`

export default class ScanScreen extends Component {

  static propTypes = {
    sampleCode: PropTypes.string,
    changeSampleCode: PropTypes.func,
    changeSampleView: PropTypes.func,
    setSuccessMessage: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      code: "",
      loading: false,
      showHelpModal: false,
    };
  }
  componentDidMount() {
    //this.scanSimulate("5a9edb0f511657494ff33c6df731c570156db57122e9e4a68738ffb71fbd1ff5")
  }

  validateCode(token, refresh_token, code) {
    // Call API to check if code is valid
    let valid = true;    // TODO: Obviously change this, it's just here for logic flow :)
    if(!valid) {
      return false;
    }
    else {
      return true;
    }
  }

  onScan(e) {
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.refs.swiper.scrollBy(1, false);
        let valid = this.validateCode(token, refresh_token, e.data);

        if(valid) {
          this.props.changeSampleCode(this.state.code);
          this.props.setSuccessMessage("Valid sample kit code scanned");
          this.props.changeSampleView("main");
        }
        else {
          // TODO
        }
      });
    });
  }
  onSubmitManual() {
    if(Keyboard)
      Keyboard.dismiss();
    if(this.state.code == "")
    {
      this.refs.ErrorMessage.show('Code cannot be left blank!', 3000);
      return;
    }
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.refs.swiper.scrollBy(2, false);
        let valid = this.validateCode(token, refresh_token, this.state.code);

        if(valid) {
          this.props.changeSampleCode(this.state.code);
          this.props.setSuccessMessage("Valid sample kit code entered");
          this.props.changeSampleView("main");
        }
        else {
          // TODO
        }
      });
    });
  }

  returnToSelection(distance) {
    this.setState({code: ""}, this.refs.swiper.scrollBy(distance, false));
  }

  showHelpModal() {
    this.setState({showHelpModal: true});
  }
  hideHelpModal() {
    this.setState({showHelpModal: false});
  }

  render () {

    return (

      <View style={styles.mainContainer}>

        <Toast
          ref="ErrorMessage"
          position='top'
          positionValue={64}
          opacity={1}
          style={{backgroundColor: Colors.error}}
        />

        <Swiper style={styles.wrapper} ref="swiper" showsButtons={false} showsPagination={false} loop={false} scrollEnabled={false}>

          {/*** Selection page ***/}
          <View style={styles.slide}>
            <View style={styles.topBarWithTitle}>
              <View style={styles.menuButtonContainer}>
                <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.props.changeSampleView("main") }>
                  <Text style={styles.centered}> <Icon name="ios-arrow-back-outline" size={35} color={Colors.snow} /> </Text>
                </TouchableOpacity>
              </View>

              <PageTitle
                title="Sample Kit Code"
              />

              {/* <View style={styles.menuButtonContainer}> */}
                <HelpModal
                  onClick={this.showHelpModal.bind(this)}
                  onClose={this.hideHelpModal.bind(this)}
                  isVisible={this.state.showHelpModal}
                  title={"Kit Code Help"}
                  text={helpText}
                  iconSize={24}>
                  <Text>{helpText}</Text>
                </HelpModal>
              {/* </View> */}

              { this.state.loading ?
                <LoadingTopBar />
                : null
              }

            </View>

            <View style={styles.contentContainer}>

              <View style={styles.centerContent}>
                <TouchableOpacity onPress={() => this.refs.swiper.scrollBy(2, false)} style={styles.largeButton}>
                  <View style={styles.largeIcon}><MaterialCommIcon name="qrcode-scan" size={50} color={Colors.snow} /></View>
                  <Text style={styles.largeButtonText}>Scan QR Code</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.refs.swiper.scrollBy(1, false)} style={styles.largeButton}>
                  <View style={styles.largeIcon}><MaterialCommIcon name="textbox" size={50} color={Colors.snow} /></View>
                  <Text style={styles.largeButtonText}>Enter Code Manually</Text>
                </TouchableOpacity>
              </View>

            </View>

          </View>

          {/*** Manual entry page ***/}
          <View style={styles.slide}>
            <View style={styles.topBarWithTitle}>
              <View style={styles.menuButtonContainer}>
                <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.returnToSelection(-1) }>
                  <Text style={styles.centered}> <Icon name="ios-arrow-back-outline" size={35} color={Colors.snow} /> </Text>
                </TouchableOpacity>
              </View>

              <PageTitle
                title="Enter Code Manually"
              />

              { this.state.loading ?
                <LoadingTopBar />
                : null
              }

            </View>

            <View style={styles.contentContainer}>

                <View style={styles.centerContentSpacing}>
                  <View style={styles.largeIcon}><MaterialCommIcon name="textbox" size={40} color={Colors.snow} /></View>
                  <Text style={styles.descriptionText}>Please enter the code located on your sample kit.</Text>
                  <KeyboardAvoidingView behavior="padding" style={{width: '100%'}}>
                    <TextInput
                      placeholder="Sample kit code"
                      placeholderTextColor={Colors.secondaryLightColor}
                      style={styles.codeInput}
                      underlineColorAndroid={'rgba(0,0,0,0)'}
                      onChangeText={(code) => this.setState({code})}
                      value={this.state.code}
                      returnKeyType="go"

                      ref='TypedCode'

                      onSubmitEditing={() => this.onSubmitManual()}
                    />

                    <View style={styles.submitCodeWrapper}>
                      <TouchableOpacity style={styles.submitButton} onPress={() => this.onSubmitManual()}>
                        <Text style={styles.submitButtonText}>Submit code</Text>
                      </TouchableOpacity>
                    </View>

                  </KeyboardAvoidingView>
                </View>
            </View>
          </View>

          {/*** QR scan page ***/}
          <View style={styles.slide}>
            <View style={styles.topBarWithTitle}>
              <View style={styles.menuButtonContainer}>
                <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.returnToSelection(-2) }>
                  <Text style={styles.centered}> <Icon name="ios-arrow-back-outline" size={35} color={Colors.snow} /> </Text>
                </TouchableOpacity>
              </View>

              <PageTitle
                title="Scan QR Code"
              />

              { this.state.loading ?
                <LoadingTopBar />
                : null
              }

            </View>

            <View style={styles.contentContainer}>

              <View style={styles.centerContentSpacing}>
                <View style={styles.largeIcon}><MaterialCommIcon name="qrcode-scan" size={40} color={Colors.snow} /></View>
                <Text style={styles.descriptionText}>Please scan the QR code located on your sample kit.</Text>
                <QRCodeScanner
                  onRead={this.onScan.bind(this)}
                />
              </View>

            </View>

          </View>

          {/* Loading screen */}
          <View style={styles.slide}>
            <View style={styles.loadingContentContainer}>
              <View style={styles.centerContent}>

                <View style={styles.spinnerWrapper}>
                  <ActivityIndicator
                    size="large"
                    color="#fff"
                    style={styles.spinner}
                  />
                </View>

                <Text style={styles.centerTextLarge}>Checking code...</Text>
              </View>
          </View>
          </View>

        </Swiper>

      </View>

    )
  }
}
