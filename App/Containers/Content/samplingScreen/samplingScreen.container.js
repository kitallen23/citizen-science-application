import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SamplingScreen from './SamplingScreen.js';
import {
    changeSampleView,
    changeSampleTitle,
    changeSampleCode,
    changeSampleDatetime,
    changeSampleLocation,
    changeSamplePhotosAmount,
    changeSampleNotesText,
  } from '../../../Utils/Sampling/sampling.actions.js';

const mapStateToProps = state => ({
  sampleView: state.sampling.sampleView,
  sampleData: state.sampling.sampleData,
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
      changeSampleView,
      changeSampleTitle,
      changeSampleCode,
      changeSampleDatetime,
      changeSampleLocation,
      changeSamplePhotosAmount,
      changeSampleNotesText,
    }, dispatch );
};

export default connect( mapStateToProps, mapDispatchToProps )( SamplingScreen );
