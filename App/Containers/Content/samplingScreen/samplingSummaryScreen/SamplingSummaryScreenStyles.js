import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  contentContainerWrapper: {
    backgroundColor: Colors.primaryColor,
    flex: 1,
  },
  contentContainer: {
    paddingHorizontal: Metrics.spacing.xl,
    marginTop: Metrics.topBarHeight,
  },
  titleInputText: {
    fontSize: Fonts.size.h3,
    color: Colors.primaryDarkerColor,
    textAlign: 'center',
  },
  titleTextLabel: {
    fontSize: Fonts.size.small,
    color: Colors.onDarkTextColorMuted,
    textAlign: 'center',
  },
  smallTextCenter: {
    fontSize: Fonts.size.normal,
    textAlign: 'center',
    color: Colors.onDarkTextColor,
  },
  sampleInnerWrapper: {
    marginBottom: 20,
    paddingHorizontal: Metrics.spacing.large,
  },

  displayTextSmall: {
    color: Colors.snow,
    fontSize: Fonts.size.normal,
  },

  // Modal styles
  dialog: {
    borderRadius: 5,
    backgroundColor: Colors.onDarkTextColor,
    height: 260,
  },
  dialogTitle: {
    backgroundColor: '#F9F9FB',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  dialogTitleText: {
    fontSize: Fonts.size.large,
    fontWeight: 'bold',
    color: '#7F7D89',
    marginVertical: Metrics.baseMargin,
    textAlign: 'center',
  },
  dialogContent: {
    paddingHorizontal: 20,
  },
  dialogDateWrapper: {
    padding: 20,
  },
  dialogTextInputWrapper: {
    height: 174,
    textAlignVertical: 'top',
    color: Colors.onLightTextColor,
    fontSize: Fonts.size.input,
  },
  buttonWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  dialogButtonLeft: {
    flexGrow: 1,
    borderBottomLeftRadius: 5,
    backgroundColor: '#F9F9FB',
  },
  dialogButtonRight: {
    flexGrow: 1,
    backgroundColor: Colors.buttonBackground,
    borderBottomRightRadius: 5,
  },
  dialogButtonText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
    color: Colors.snow,
  },
  dialogButtonTextDark: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
    color: '#7F7D89',
  },
  dialogTextSmallCenter: {
    fontSize: Fonts.size.normal,
    textAlign: 'center',
    color: Colors.onLightTextColor,
  },
  dialogTextLargeCenter: {
    fontSize: Fonts.size.large,
    textAlign: 'center',
    color: Colors.onLightTextColor,
  },

  submitButton: {
    flex: 1,
    alignItems: 'center',
    padding: Metrics.spacing.medium,
    borderRadius: 5,
    backgroundColor: Colors.success,
    elevation: 1,
    marginVertical: Metrics.spacing.xl,
  },
  submitButtonMuted: {
    flex: 1,
    alignItems: 'center',
    padding: Metrics.spacing.medium,
    borderRadius: 5,
    backgroundColor: Colors.muted,
    elevation: 1,
    marginVertical: Metrics.spacing.xl,
  },
  submitButtonText: {
    color: Colors.snow,
    fontSize: Fonts.size.normal,
    fontWeight: "bold",
    marginHorizontal: Metrics.spacing.medium,
  },
  submitButtonContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

})
