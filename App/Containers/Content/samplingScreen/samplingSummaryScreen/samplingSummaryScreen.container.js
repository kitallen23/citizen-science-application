import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SamplingSummaryScreen from './SamplingSummaryScreen.js';
import {
    changeSampleView,
    changeSampleTitle,
    changeSampleCode,
    changeSampleDatetime,
    changeSampleLocation,
    changeSamplePhotosAmount,
    changeSampleNotesText,
    toggleSampleCodeExpanded,
    toggleSampleDatetimeExpanded,
    toggleSampleLocationExpanded,
    toggleSamplePhotosExpanded,
    toggleSampleNotesExpanded,
    clearSuccessMessage,
  } from '../../../../Utils/Sampling/sampling.actions.js';
import { changeContent } from '../../../../Utils/Navigation/navigation.actions.js';

const mapStateToProps = state => ({
  sampleView: state.sampling.sampleView,
  sampleData: state.sampling.sampleData,
  expanded: state.sampling.expanded,
  successMessage: state.sampling.successMessage,
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
      changeSampleView,
      changeSampleTitle,
      changeSampleCode,
      changeSampleDatetime,
      changeSampleLocation,
      changeSamplePhotosAmount,
      changeSampleNotesText,
      toggleSampleCodeExpanded,
      toggleSampleDatetimeExpanded,
      toggleSampleLocationExpanded,
      toggleSamplePhotosExpanded,
      toggleSampleNotesExpanded,
      clearSuccessMessage,
      changeContent,
    }, dispatch );
};

export default connect( mapStateToProps, mapDispatchToProps )( SamplingSummaryScreen );
