import React, { Component } from 'react';
import { Keyboard, TextInput, Alert, Button, BackHandler, TouchableOpacity, Text, View, Image, ScrollView, AsyncStorage } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import Modal from 'react-native-modal';
import Toast, { DURATION } from 'react-native-easy-toast';

import PageTitle from '../../../../Components/PageTitle.js';
import SampleCategory from '../../../../Components/SampleCategory.js';

// Styles
import styles from './SamplingSummaryScreenStyles';
import { Colors, Images, Metrics, Fonts } from '../../../../Themes/';

/**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info below in README)
 */
var day = new Date();
var dayWrapper = moment(day);
var maxDate = dayWrapper.toDate();
var minDate = dayWrapper.subtract(1, 'weeks').toDate();


export default class SamplingScreen extends Component {

  static propTypes = {
    sampleView: PropTypes.string,
    changeSampleView: PropTypes.func,

    sampleData: PropTypes.object,
    changeSampleTitle: PropTypes.func,
    changeSampleDatetime: PropTypes.func,
    changeSampleNotesText: PropTypes.func,

    expanded: PropTypes.object,
    toggleSampleCodeExpanded: PropTypes.func,
    toggleSampleDatetimeExpanded: PropTypes.func,
    toggleSampleLocationExpanded: PropTypes.func,
    toggleSamplePhotosExpanded: PropTypes.func,
    toggleSampleNotesExpanded: PropTypes.func,

    successMessage: PropTypes.object,
    clearSuccessMessage: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      date: "",
      time: "",
      notesTextTemp: "",
      buttonText: "0/6",

      showDateModal: false,
      showNotesModal: false,

      timeFormat: "12h",
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.props.goBack);

    if(this.props.sampleData.datetime !== "")
      this.updateDateTime(this.props.sampleData.datetime);
    this.updateButtonText(this.props);

    if(this.props.successMessage.show == true)
    {
      this.props.clearSuccessMessage();
      this.refs.SuccessMessage.show(this.props.successMessage.text, 3000);
    }
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.props.goBack);
  }
  componentWillReceiveProps(nextProps) {
    if(this.props.sampleData.datetime !== nextProps.sampleData.datetime)
    {
      this.updateDateTime(nextProps.sampleData.datetime);
    }
    this.updateButtonText(nextProps);
  }

  confirmGoBack() {
    // Check that all entries are blank
    if(this.updateButtonText(this.props) !== 0)
    {
      Alert.alert(
        "Cancel sample process",
        "Are you sure you wish to cancel the sample process? Any information previously entered will be lost.",
        [
          {text: 'Stay on page'},
          {text: 'Discard & exit', onPress: () => this.goBack() },
        ],
        { cancelable: false }
      );
    }
    else
    {
      this.goBack();
    }
  }
  goBack() {
    this.props.changeContent("Map");
  }

  updateDateTime(date) {
    // TODO: Update this to reflect the setting in redux store
    this.setState({
        date: moment(date).format("D MMMM YYYY"),
        time: this.getTime(date),
      });
  }
  getTime(date) {
    if(this.state.timeFormat == "12h")
      return moment(date).format("h:mma");
    return moment(date).format("HH:mm");
  }

  updateButtonText(_props) {
    let filled = 0;
    if(_props.sampleData.title !== "")
      ++filled;
    if(_props.sampleData.code !== "")
      ++filled;
    if(_props.sampleData.datetime !== "")
      ++filled;
    if(_props.sampleData.location.lat !== null &&
        _props.sampleData.location.lng !== null)
      ++filled;
    if(_props.sampleData.photosAmount !== 0)
      ++filled;
    if(_props.sampleData.notesText !== "")
      ++filled;
    console.log("Filled: " + filled);

    this.setState({buttonText: filled !== 6 ? filled + "/6" : "Submit"});
    // this.setState({buttonText: "Submit"});
    // this.forceUpdate();

    return filled;
  }

  /****************************************************************************/
  /*** Show/Hide Methods ******************************************************/
  /****************************************************************************/
  toggleDateExpanded() {
    if(this.props.sampleData.datetime !== "")
      this.props.toggleSampleDatetimeExpanded();
  }
  hideDateTimePicker() {
    this.setState({showDateModal: false});
  }
  showDateTimePicker() {
    this.setState({showDateModal: true});
  }

  toggleNotesExpanded() {
    if(this.props.sampleData.notesText !== "")
      this.props.toggleSampleNotesExpanded();
  }
  openNotesModal() {
    this.setState({
      notesTextTemp: this.props.sampleData.notesText,
      showNotesModal: true,
    });
  }

  toggleCodeExpanded() {
    if(this.props.sampleData.code !== "")
      this.props.toggleSampleCodeExpanded();
  }

  /****************************************************************************/
  /*** Data Handlers **********************************************************/
  /****************************************************************************/
  handleTitleChange(text) {
    this.props.changeSampleTitle(text);
  }
  handleDatePicked(date) {
    if(date.valueOf() > new Date().valueOf()){
      this.refs.ErrorMessage.show('Time must not be in the future!', 3000);
    }
    else {
      this.props.changeSampleDatetime(date);
    }
    this.hideDateTimePicker();
  }

  discardNotesModal() {
    Keyboard.dismiss();
    this.setState({
      notesTextTemp: "",
      showNotesModal: false,
    });
  }
  saveNotesModal() {
    Keyboard.dismiss();
    // this.refs.NotesInput.blur();
    this.props.changeSampleNotesText(this.state.notesTextTemp);

    this.setState({
      showNotesModal: false,
      notesTextTemp: "",
    });
  }

  render () {

    return (

      <View style={styles.mainContainer}>

        <Toast
          ref="ErrorMessage"
          position='top'
          positionValue={64}
          opacity={1}
          style={{backgroundColor: Colors.error}}
        />
        <Toast
          ref="SuccessMessage"
          position='top'
          positionValue={64}
          opacity={1}
          style={{backgroundColor: Colors.success}}
        />

        <View style={styles.topBarWithTitle}>
          <View style={styles.menuButtonContainer}>
            <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.confirmGoBack() }>
              <Text style={styles.centered}> <Icon name="ios-arrow-back-outline" size={35} color={Colors.snow} /> </Text>
            </TouchableOpacity>
          </View>

          <PageTitle
            title="Record sample"
          />
        </View>

        <View style={styles.contentContainerWrapper}>
          <ScrollView style={styles.contentContainer} keyboardShouldPersistTaps="always">

            <View style={{marginVertical: 20}}>
              <Text style={styles.smallTextCenter}>Please fill out each entry below as accurately as possible to record your sample.</Text>
            </View>

            {/*** Title ***/}
            <Text style={styles.titleTextLabel}>Sample title:</Text>
            <TextInput
              placeholder="Add Title +"
              placeholderTextColor={"rgba(255,255,255,0.6)"}
              style={styles.titleInputText}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              autoCorrect={true}
              autoCapitalize="words"
              returnKeyType="done"
              value={this.props.sampleData.title}
              onChangeText={(title) => this.handleTitleChange(title)}

              ref="TitleInput"
            />

            {/*** QR code ***/}
            <SampleCategory
              title="Sample Kit Code"
              isFilled={this.props.sampleData.code !== ""}
              isExpanded={this.props.expanded.codeExpanded}
              toggleDisplay={() => this.toggleCodeExpanded()}
              onClick={() => this.props.changeSampleView("code")}
              >
                { this.props.sampleData.code !== "" ? (
                  <View style={styles.sampleInnerWrapper}>
                    <Text style={styles.displayTextSmall}>{this.props.sampleData.code}</Text>
                    <Text style={styles.displayTextSmall}></Text>
                  </View>
                ) : (
                  null
                )}
            </SampleCategory>

            {/*** Date & time ***/}
            <DateTimePicker
              isVisible={this.state.showDateModal}
              onConfirm={(date) => this.handleDatePicked(date)}
              onCancel={() => this.hideDateTimePicker()}
              minimumDate={minDate}
              maximumDate={day}
              mode="datetime"
              is24Hour={false}
            />

            <SampleCategory
              title="Date & Time"
              isFilled={this.props.sampleData.datetime !== ""}
              isExpanded={this.props.expanded.dateExpanded}
              toggleDisplay={() => this.toggleDateExpanded()}
              onClick={() => this.showDateTimePicker()}
              >
                { this.props.sampleData.datetime !== "" ? (
                  <View style={styles.sampleInnerWrapper}>
                    <Text style={styles.displayTextSmall}>{this.state.date} at {this.state.time}</Text>
                    <Text style={styles.displayTextSmall}></Text>
                  </View>
                ) : (
                  null
                )}
            </SampleCategory>

            {/*** TODO: Location ***/}
            <SampleCategory title="Location"></SampleCategory>

            {/*** TODO: Photos ***/}
            <SampleCategory title="Photo(s)"></SampleCategory>

            {/*** Additional notes ***/}
            <Modal
              isVisible={this.state.showNotesModal}
              animationIn={'slideInUp'}
              animationOut={'slideOutDown'}
              onBackdropPress={() => this.discardNotesModal()}
              onBackButtonPress={() => this.discardNotesModal()}
              ref={(notesDialog) => { this.notesDialog = notesDialog; }}>
              <View style={styles.dialog}>

                <View style={styles.dialogTitle}>
                  <Text style={styles.dialogTitleText}>Additional Notes</Text>
                </View>

                <View style={styles.dialogContent}>
                  <TextInput
                    placeholder="Write any relevant notes here..."
                    style={styles.dialogTextInputWrapper}
                    underlineColorAndroid={'rgba(0,0,0,0)'}
                    multiline={true}
                    autoCorrect={true}
                    autoCapitalize="sentences"
                    autogrow={false}
                    returnKeyType="done"
                    value={this.state.notesTextTemp}
                    onChangeText={(notesTextTemp) => this.setState({notesTextTemp})}

                    ref="NotesInput"
                  />
                </View>

                <View style={styles.buttonWrapper}>

                  <View style={styles.dialogButtonLeft}>
                    <TouchableOpacity onPress={() => this.discardNotesModal()}>
                      <Text style={styles.dialogButtonTextDark}>Cancel</Text>
                    </TouchableOpacity>
                  </View>

                  <View style={styles.dialogButtonRight}>
                    <TouchableOpacity onPress={() => this.saveNotesModal()}>
                      <Text style={styles.dialogButtonText}>Save</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
            </Modal>

            <SampleCategory
              title="Additional Notes"
              isFilled={this.props.sampleData.notesText !== ""}
              isExpanded={this.props.expanded.notesExpanded}
              toggleDisplay={() => this.toggleNotesExpanded()}
              onClick={() => this.openNotesModal()}
              >
                { this.props.sampleData.notesText !== "" ? (
                  <View style={styles.sampleInnerWrapper}>
                    <Text style={styles.displayTextSmall}>{this.props.sampleData.notesText}</Text>
                  </View>
                ) : (
                  null
                )}
              </SampleCategory>


              {this.state.buttonText == "Submit" ? (
                //* TODO: Replace this with the proper submit method
                <TouchableOpacity style={styles.submitButton} onPress={ () => this.confirmGoBack() }>
                  <View style={styles.submitButtonContentContainer}>
                    <Text style={styles.submitButtonText}>{this.state.buttonText}</Text>
                    <MaterialCommIcon name="check" style={styles.icon} size={20} color={Colors.snow} />
                  </View>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={styles.submitButtonMuted}
                  onPress={ () => this.refs.ErrorMessage.show('Please fill out all sample data fields.', 3000) }>
                  <View style={styles.submitButtonContentContainer}>
                    <Text style={styles.submitButtonText}>{this.state.buttonText}</Text>
                  </View>
                </TouchableOpacity>
              )}

          </ScrollView>
        </View>
      </View>

    )
  }
}
