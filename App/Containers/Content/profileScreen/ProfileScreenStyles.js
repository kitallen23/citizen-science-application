import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
  },
  refreshButton: {
    backgroundColor: Colors.buttonBackground,
    marginTop: 15,
    borderRadius: 5,
  },
  refreshText: {
    textAlign: 'center',
    padding: 15,
    fontSize: Fonts.size.normal,
    color: Colors.snow,
  },
  background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    resizeMode: 'stretch',
  },
  profileContentContainer: {
    paddingTop: Metrics.topBarHeight,
    paddingHorizontal: Metrics.spacing.xl,
  },
  profileRow: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  profileSubContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingVertical: Metrics.spacing.xl,
  },
  profileTextHeader: {
    fontSize: Fonts.size.normal,
    color: Colors.onLightTextColor,
    fontWeight: 'bold',
    flex: 1,
    marginBottom: Metrics.spacing.medium,
  },
  profileTextSmallHeader: {
    fontSize: Fonts.size.normal,
    paddingVertical: Metrics.spacing.tiny,
    color: Colors.onLightTextColor,
    flex: 1,
  },
  profileTextSmallHeaderMuted: {
    fontSize: Fonts.size.normal,
    paddingVertical: Metrics.spacing.tiny,
    color: Colors.onLightTextColorMuted,
    flex: 1,
  },
  profileTextSmallLeft: {
    fontSize: Fonts.size.input,
    paddingVertical: Metrics.spacing.tiny,
    color: Colors.onLightTextColor,
    flex: 1,
    textAlign: 'left',
  },
  profileTextSmallRight: {
    fontSize: Fonts.size.input,
    paddingVertical: Metrics.spacing.tiny,
    color: Colors.onLightTextColor,
    flex: 1,
    textAlign: 'right',

  },

  editButton: {
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.transparent,
    marginBottom: Metrics.spacing.medium,
  },
  editButtonText: {
    color: Colors.snow,
    fontSize: Fonts.size.normal,
  },
})
