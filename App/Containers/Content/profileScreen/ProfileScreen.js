import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Alert, AsyncStorage, TouchableOpacity, Text, View, Image, Keyboard } from 'react-native';
import Toast, { DURATION } from 'react-native-easy-toast';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import PageTitle from '../../../Components/PageTitle.js';
import LoadingTopBar from '../../../Components/LoadingTopBar.js';
import EditPersonalDialog from '../../../Components/EditPersonalDialog';
import EditPostalDialog from '../../../Components/EditPostalDialog';

// Styles
import styles from './ProfileScreenStyles';
import { Colors, Images } from '../../../Themes/';

//
//
//
//
//
//
//
//
// IMPORTANT TODO: CHANGE THIS ENTIRE SCREEN TO USE A SCROLLVIEW
//
//
//
//
//
//
//
//

export default class ProfileScreen extends Component {

 static propTypes = {
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      address: {
        streetAddress: "",
        city: "",
        postcode: "",
        state: "",
      },
      numSamples: 0,
      loading: true,
      loadSuccess: false,
      showPersonalDialog: false,
      showPostalDialog: false,
    };
  }

  componentDidMount() {
    this.loadProfile()
  }

  closePersonalDialog() {
    this.setState({showPersonalDialog: false});
  }
  closePostalDialog() {
    this.setState({showPostalDialog: false});
  }

  goHome() {
    this.props.goHome();
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }
  changeContent(screen){
    this.props.changeContent(screen);
  }

  loadProfile() {
    // load profile from local storage
    AsyncStorage.getItem("account:name").then((name) => {
    AsyncStorage.getItem("account:email").then((email) => {
    AsyncStorage.getItem("account:streetAddress").then((streetAddress) => {
    AsyncStorage.getItem("account:city").then((city) => {
    AsyncStorage.getItem("account:postcode").then((postcode) => {
    AsyncStorage.getItem("account:state").then((state) => {
      this.setState({name: name});
      this.setState({email: email});
      this.setState({
        address: {
          ...this.state.address,
          streetAddress: streetAddress,
          city: city,
          postcode: postcode,
          state: state,
        }
      });
      // load profile from api
      AsyncStorage.getItem("token").then((token) => {
        AsyncStorage.getItem("refresh_token").then((refresh_token) => {
          this.getProfile(token, refresh_token);
        });
      });

    });
    });
    });
    });
    });
    });
  }

  getProfile(token, refresh_token) {
    this.setState({loading: true})
    fetch('https://citsciapp.herokuapp.com/profile', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: token,
        refresh_token: refresh_token,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log("Response:");
      console.log(responseJson);

      if (responseJson.status == "SUCCESS") {
        AsyncStorage.setItem("token", responseJson.token);
        AsyncStorage.setItem("refresh_token", responseJson.refresh_token);

        // Ensure our AsyncStorage is up to date
        if(this.state.address !== responseJson.address)
        {
          AsyncStorage.setItem("account:streetAddress", responseJson.address.streetAddress).then(() => {
          AsyncStorage.setItem("account:city", responseJson.address.city).then(() => {
          AsyncStorage.setItem("account:postcode", responseJson.address.postcode).then(() => {
          AsyncStorage.setItem("account:state", responseJson.address.state).then(() => {
          });
          });
          });
          });
        }
        if(this.state.name !== responseJson.name)
        {
          AsyncStorage.setItem("account:name", responseJson.name);
        }
        if(this.state.email !== responseJson.email)
        {
          AsyncStorage.setItem("account:name", responseJson.name);
        }
        this.setState({name: responseJson.name});
        this.setState({email: responseJson.email});
        this.setState({address: responseJson.address});
        this.setState({loadSuccess: true});
      }
      else {
        Alert.alert(
          "Error when retrieving profile",
          responseJson.message,
          [
            {text: 'Log in', onPress: () => this.logout() },
          ],
          { cancelable: false }
        );
      }
      this.setState({loading: false});
      return;
    })
    .catch((error) => {
      this.setState({loading: false});
      Alert.alert(
        "Error when retrieving profile",
        "Could not receive response from server, please check your internet connection and try again.",
        [
          {text: 'Log in', onPress: () => this.logout()},
        ],
        { cancelable: false }
      )
      console.error(error);
    });
  }

  logout() {
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.setState({loading: true});

        fetch('https://citsciapp.herokuapp.com/logout', {
          method: 'POST',
          headers:  {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            token: token,
            refresh_token: refresh_token,
          })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({loading: false});
        })
        .catch((error) => {
          console.error(error);
        });
      });
    });
    AsyncStorage.removeItem("token").then(() => {
      AsyncStorage.removeItem("refresh_token").then(() => {
        // Navigate to login screen
        this.props.goLogin();
        console.log("Logged out");
      });
    });
  }

  setPersonalDetails(t_name, t_email) {
    this.setState({
      name: t_name,
      email: t_email,
    });
    this.refs.editSuccess.show('Personal details updated!', 3000);
  }
  setPostalDetails(t_address) {
    this.setState({address: t_address});
    this.refs.editSuccess.show('Postal address updated!', 3000);
  }

  render () {

    return (

      <View style={styles.mainContainer}>

        <View style={styles.topBarWithTitle}>
          <View style={styles.menuButtonContainer}>
            <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.showDrawer() }>
              <Text style={styles.centered}> <Icon name="ios-menu" size={35} color={Colors.snow} /> </Text>
            </TouchableOpacity>
          </View>

          <PageTitle
            title="My Profile"
          />

          { this.state.loading ?
            <LoadingTopBar />
            : null
          }

        </View>

        <Toast
          ref="editSuccess"
          position='top'
          positionValue={64}
          opacity={1}
          style={{backgroundColor: '#4BBF73'}}
        />
        {this.state.loadSuccess ? (
          <EditPersonalDialog
            show={this.state.showPersonalDialog}
            onDismiss={() => this.closePersonalDialog()}
            name={this.state.name}
            email={this.state.email}
            onSuccess={this.setPersonalDetails.bind(this)}
          />
        ) : null }
        {this.state.loadSuccess ? (
          <EditPostalDialog
            show={this.state.showPostalDialog}
            onDismiss={() => this.closePostalDialog()}
            address={this.state.address}
            onSuccess={this.setPostalDetails.bind(this)}
          />
        ) : null }

        <View style={styles.profileContentContainer}>

          <View style={styles.profileSubContainer}>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextHeader}>Personal Details</Text>

              {this.state.loadSuccess ? (
                <TouchableOpacity style={styles.editButton} onPress={ () => this.setState({showPersonalDialog: true}) }>
                  <MaterialIcon name="edit" size={20} color={Colors.primaryDarkColor} />
                </TouchableOpacity>
              ) : (
                <View style={styles.editButton}>
                  <MaterialIcon name="edit" size={20} color={Colors.primaryDarkColorDisabled} />
                </View>
              )}
            </View>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextSmallHeaderMuted}>Name:</Text>
              <Text style={styles.profileTextSmallRight}>{this.state.name}</Text>
            </View>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextSmallHeaderMuted}>Email:</Text>
              <Text style={styles.profileTextSmallRight}>{this.state.email}</Text>
            </View>

          </View>

          <View style={styles.profileSubContainer}>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextHeader}>Postal Address</Text>

              {this.state.loadSuccess ? (
                <TouchableOpacity style={styles.editButton} onPress={ () => this.setState({showPostalDialog: true}) }>
                  <MaterialIcon name="edit" size={20} color={Colors.primaryDarkColor} />
                </TouchableOpacity>
              ) : (
                <View style={styles.editButton}>
                  <MaterialIcon name="edit" size={20} color={Colors.primaryDarkColorDisabled} />
                </View>
              )}
            </View>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextSmallLeft}>{this.state.address.streetAddress}</Text>
            </View>

            <View style={styles.profileRow}>
                <Text style={styles.profileTextSmallLeft}>{this.state.address.city}</Text>
            </View>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextSmallLeft}>{this.state.address.state} {this.state.address.postcode}</Text>
            </View>

          </View>

          <View style={styles.profileSubContainer}>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextHeader}>Other Details</Text>
            </View>

            <View style={styles.profileRow}>
              <Text style={styles.profileTextSmallHeader}>Total samples taken:</Text>
              <Text style={styles.profileTextSmallRight}>{this.state.numSamples}</Text>
            </View>

          </View>

          <TouchableOpacity style={styles.refreshButton} onPress={ () => this.loadProfile()} disabled={this.state.loading}>
            <Text style={styles.refreshText}>Reload Profile Data</Text>
          </TouchableOpacity>

        </View>

      </View>

    )
  }
}
