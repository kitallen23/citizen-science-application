import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
  },
  centerContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.ricePaper,
    paddingVertical: 80
  },
  marginContent: {
    flex: 1,
    backgroundColor: Colors.ricePaper,
    paddingVertical: 80,
    paddingHorizontal: 30
  },
  scrollContent: {
    flex: 1,
  },
  textLarge: {
    color: Colors.charcoal,
    marginBottom: Metrics.spacing.large,
    fontSize: Fonts.size.h3,
    fontWeight: 'bold',
  },
  text: {
    color: Colors.charcoal,
    marginBottom: Metrics.spacing.large,
    fontSize: Fonts.size.normal,
  },
  photos: {
    height: 200, 
    width: Metrics.screenWidth, 
    resizeMode: 'contain'
  },
  photoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20
  }
})
