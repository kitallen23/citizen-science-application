import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, AsyncStorage, TouchableOpacity, Text, View, Image } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import PageTitle from '../../../Components/PageTitle.js';
import LoadingTopBar from '../../../Components/LoadingTopBar.js';
import QRCodeScanner from '../../../Components/QRCodeScanner.js';

// Styles
import styles from './ScanScreenStyles'
import { Colors, Images } from '../../../Themes/'

APIKey = "AIzaSyAAIww5AeJtDGgbAiWzcfKmX_WZ0gA75GU"

export default class ScanScreen extends Component {

 static propTypes = {
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      code: "",
      loading: false,
      data: [],
      locationName: "",
    };
  }

  goHome() {
    this.props.goHome();
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }
  onScan(e) {
    this.setState({code: e.data});
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.getSample(token, refresh_token, e.data);
      });
    });
  }
  scanSimulate(e) {
    this.setState({code: e});
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.getSample(token, refresh_token, e);
      });
    });
  }
  getSample(token, refresh_token, code) {
    this.setState({loading: true})
    fetch('https://citsciapp.herokuapp.com/scan', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: token,
        refresh_token: refresh_token,
        kitcode: code
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {

      if (responseJson.status == "SUCCESS") {
        AsyncStorage.setItem("token", responseJson.token);
        AsyncStorage.setItem("refresh_token", responseJson.refresh_token);
        this.setState({data: responseJson.data});
        this.getLocationName(responseJson.data.lat, responseJson.data.lng)
      }
      this.setState({loading: false});
      return;
    })
    .catch((error) => {
      console.error(error);
    });
  }
  getLocationName(lat,long) {
    var latlng = `${lat},${long}`
    fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${APIKey}`)
      .then( (res) => res.json() )
      .then( (json) => {
        if (json.status == 'OK') {
          locationName = json.results[0].formatted_address
          this.setState({locationName: locationName})
        }
      });
  }
  componentDidMount() {
    //this.scanSimulate("f83515fab9f9151a6f4adc9d8bff5185c18fcd7051e9772ba8a27560030c4206")
  }


  render () {

    return (

      <View style={styles.mainContainer}>

        <View style={styles.topBarWithTitle}>
          <View style={styles.menuButtonContainer}>
            <TouchableOpacity style={styles.menuButtonWithTitle} onPress={ () => this.showDrawer() }>
              <Text style={styles.centered}> <Icon name="ios-menu" size={35} color={Colors.snow} /> </Text>
            </TouchableOpacity>
          </View>

          <PageTitle
            title="View Sample"
          />

          { this.state.loading ?
            <LoadingTopBar />
            : null
          }

        </View>

        { this.state.data.length === 0 ?
          <View style={styles.centerContent}>
            <Text style={styles.text}>Scan the sample QR code</Text>
            <QRCodeScanner
              onRead={this.onScan.bind(this)}
              />
          </View>
        : 
          <ScrollView style={styles.scrollContent}>
            <View style={styles.marginContent}>
              {this.state.data.photos.map((photo,i) =>
                <View key={i} style={styles.photoContainer}>
                  <Image style={styles.photos} source={{uri: photo }} />
                </View>
              )}

              <Text style={styles.textLarge}>{ this.state.data.name }</Text>
              <Text style={styles.text}>Captured: { this.state.data.captureTime }</Text>
              <Text style={styles.text}>Approximate Address: { this.state.locationName }</Text>

              <Text style={styles.textLarge}>Notes</Text>
              <Text style={styles.text}>{ this.state.data.notes }</Text>

              <Text style={styles.textLarge}>Weather</Text>
              <Text style={styles.text}>{ this.state.data.weather.desc }</Text>
              <Text style={styles.text}>{ this.state.data.weather.temperature }°C</Text>
              <Text style={styles.text}>{ this.state.data.weather.cloudCover }% Cloud Cover </Text>
              <Text style={styles.text}>{ this.state.data.weather.windSpeed } Meter/sec </Text>
              <Text style={styles.text}>{ this.state.data.weather.windDirection } Degrees </Text>
            
              <Text style={styles.textLarge}>Coordinates</Text>
              <Text style={styles.text}>Latitude: { this.state.data.lat }</Text>
              <Text style={styles.text}>Longitude: { this.state.data.lng }</Text>

              <Text style={styles.textLarge}>Matrix</Text>
              <Text style={styles.text}>{ this.state.data.matrix }</Text>

              <Text style={styles.textLarge}>Cycle Number</Text>
              <Text style={styles.text}>{ this.state.data.cycleNumber }</Text>

              <Text style={styles.textLarge}>pH Level</Text>
              <Text style={styles.text}>{ this.state.data.ph }</Text>

            </View>
          </ScrollView>
        }


      </View>

    )
  }
}
