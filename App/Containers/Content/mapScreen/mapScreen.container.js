import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MapScreen from './MapScreen.js';
import { setDrawer, changeContent } from '../../../Utils/Navigation/navigation.actions.js';
import { changePosition } from '../../../Utils/Map/map.actions.js';

const mapStateToProps = state => ({
  position: state.map.position
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ setDrawer, changeContent, changePosition }, dispatch );
};


export default connect( mapStateToProps, mapDispatchToProps )( MapScreen );
