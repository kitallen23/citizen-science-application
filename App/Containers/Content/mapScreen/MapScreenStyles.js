import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
  },
  topleft: {
    position: "absolute",
    left: 0,
    width: 50,
    height: 50,
    zIndex: 1,
    backgroundColor: Colors.buttonBackground,
    elevation: 1,
  },
  buttonPadding: {
    padding: 10,
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centeredHorizontal: {
    alignItems: 'center'
  },
  map: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: Colors.windowTint,
  },
  textLarge: {
    color: Colors.windowTint,
    fontSize: 20
  },
  searchContainer: {
    margin: 5,
    flex: 1,
  },
  search: {
    borderRadius: 5,
    color: Colors.secondaryTextColor,
    padding: Metrics.spacing.large,
    fontSize: Fonts.size.normal,
    backgroundColor: Colors.snow,
    width: "100%",
    height: 50,
    elevation: 2,
  },
  searchResult: {
    fontSize: 18,
    color: "black",
    paddingVertical: 10,
  },
  resultsContainer: {
    position: "absolute",
    top: 70,
    right: 0,
    left: 0,
    paddingHorizontal: 10,
    marginHorizontal: Metrics.marginHorizontal,
    zIndex: 2,
    borderRadius: 5,
    backgroundColor: Colors.snow,
    elevation: 3,
    bottom: 10,
  },

  mainButtonsContainer: {
    position: "absolute",
    padding: Metrics.spacing.medium,
    bottom: Metrics.spacing.xl,
    right: 0,
    left: 0,
    justifyContent: 'space-between',
    display: 'flex',
    flexDirection: 'row',
    zIndex: 1,
  },
  button: {
    paddingVertical: Metrics.spacing.large,
    paddingHorizontal: Metrics.spacing.xl,
    margin: Metrics.spacing.medium,
    backgroundColor: Colors.buttonBackground,
    borderRadius: 3,
    elevation: 1,
    width: "45%",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontWeight: 'bold',
    color: Colors.snow,
    fontSize: Fonts.size.normal,
    textAlign: 'center',
  },
})
