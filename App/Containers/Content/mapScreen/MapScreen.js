import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { AsyncStorage, BackHandler, ScrollView, Keyboard, ListView, Alert, TouchableOpacity, Text, Image, View, TextInput } from 'react-native'
import RoundedButton from '../../../Components/RoundedButton.js'
import SampleMap from '../../../Components/SampleMap.js'
import Icon from 'react-native-vector-icons/Ionicons';
import Geocoder from 'react-native-geocoding';
import LoadingTopBar from '../../../Components/LoadingTopBar.js';

// Styles
import { Colors, Images } from '../../../Themes/'
import styles from './MapScreenStyles'

APIKey = "AIzaSyAAIww5AeJtDGgbAiWzcfKmX_WZ0gA75GU"
Geocoder.setApiKey(APIKey);

export default class MapScreen extends Component {
  
  static propTypes = {
    position: PropTypes.object,
    setDrawer: PropTypes.func,
    changeContent: PropTypes.func,
    changePosition: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      searchValue: "",
      searchResults: [],
      ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      markers: [],
      loading: true,
    };
  }

  handleBackPress() {
    if (this.state.searchResults.length > 0) {
      this.setState({searchValue: "", searchResults: []})
    }
    else {
      BackHandler.exitApp();
    }
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress.bind(this));

    this.loadSamples()
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  handleRegionChange(region) {
    this.props.changePosition(region)
  }

  getMarkers(data) {
    markers = []
    data.forEach(function(marker,index) {
      if (marker.photo == "") {
        uri = null
      } else {
        uri = marker.photo
      }
      markers[index] = {
        "title": "Sample no. "+marker.id,
        "latitude": parseFloat(marker.lat),
        "longitude": parseFloat(marker.lng),
        "image": {uri: uri},
      }
    })
    if (this.props.getContent == "Map") {
      this.setState({ markers: markers })
      this.setState({ loading: false });
    }
  }

  loadSamples() {
    this.setState({ loading: true });
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.getSamples(token, refresh_token);
      });
    });
  }

  getSamples(token, refresh_token) {
    fetch('https://citsciapp.herokuapp.com/samples', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // Do login token shit
        token: token,
        refresh_token: refresh_token,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {

      if (responseJson.status == "SUCCESS") {
        AsyncStorage.setItem("token", responseJson.token);
        AsyncStorage.setItem("refresh_token", responseJson.refresh_token);
        this.getMarkers(responseJson.data);
      }
      else {
        Alert.alert(
          "Error when retrieving samples",
          responseJson.message,
          [
            {text: 'Log in', onPress: () => this.logout() },
          ],
        );
      }
      this.setState({loading: false});
      return;
    })
    .catch((error) => {
      this.setState({loading: false});
      Alert.alert(
        "Error when retrieving samples",
        "Could not receive response from server, please check your internet connection and try again.",
        [
          {text: 'Log in', onPress: () => this.logout() },
        ],
        { cancelable: false }
      );
      console.error(error);
    });
  }

  logout() {
    AsyncStorage.getItem("token").then((token) => {
      AsyncStorage.getItem("refresh_token").then((refresh_token) => {
        this.setState({loading: true});

        fetch('https://citsciapp.herokuapp.com/logout', {
          method: 'POST',
          headers:  {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            token: token,
            refresh_token: refresh_token,
          })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({loading: false});
        })
        .catch((error) => {
          console.error(error);
        });
      });
    });
    AsyncStorage.removeItem("token").then(() => {
      AsyncStorage.removeItem("refresh_token").then(() => {
        // Navigate to login screen
        this.props.goLogin();
        console.log("Logged out");
      });
    });
  }

  getLocation(location) {
    Geocoder.getFromLocation(location).then(
      json => {
        var location = json.results[0].geometry.location;
        var newRegion = {}
        newRegion.latitude = location.lat
        newRegion.longitude = location.lng
        newRegion.latitudeDelta = this.state.region.latitudeDelta
        newRegion.longitudeDelta = this.state.region.longitudeDelta
        this.handleRegionChange(newRegion);
      },
      error => {
        alert("Could not find: "+location);
      }
    );
  }

  getResults(location) {
    var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+location+"&types=geocode&language=en&key="+APIKey;

    return fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({searchResults: responseJson.predictions})
        return;
      })
      .catch((error) => {
        console.error(error);
      });

  }

  handleSearchInput(input){
    this.setState({searchValue: input})
    this.getResults(input);
  }
  selectResult(result){
    this.setState({searchValue: result})
    this.getLocation(result);
    Keyboard.dismiss();
    this.setState({searchResults: []})
  }
  handleSearchBlue(){
    this.setState({searchValue: ""})
    this.setState({searchResults: []})
  }

  showDrawer(){
    this.props.setDrawer(true);
  }
  closeDrawer(){
    this.props.setDrawer(false);
  }
  changeContent(screen){
    this.props.changeContent(screen);
  }

  render () {

    return (

        <View style={styles.mainContainer}>

          <View style={styles.topBar}>
            <View style={styles.menuButtonContainer}>
              <TouchableOpacity style={styles.menuButton} onPress={ () => this.showDrawer() }>
              <Text style={styles.centered}> <Icon name="ios-menu" size={35} color={Colors.snow} /> </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.searchContainer}>
              <TextInput
                selectTextOnFocus={true}
                style={styles.search}
                returnKeyLabel="Search"
                keyboardType="web-search"
                placeholder="Search..."
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.searchValue}
                onChangeText={(searchValue) => this.handleSearchInput(searchValue)}
                onSubmitEditing={() => this.getLocation(this.state.searchValue)}
                onBlur={() => this.handleSearchBlue()}
                />
            </View>

            <View style={styles.menuButtonContainer}>
              <TouchableOpacity style={styles.menuButton} onPress={ () => this.changeContent('List') }>
              <Text style={styles.centered}> <Icon name="ios-list-box-outline" size={35} color={Colors.snow} /> </Text>
              </TouchableOpacity>
            </View>

          </View>

          {this.state.searchResults.length > 0 &&
            <ScrollView style={styles.resultsContainer} keyboardShouldPersistTaps="always">
              <ListView
                keyboardShouldPersistTaps="always"
                dataSource={this.state.ds.cloneWithRows(this.state.searchResults)}
                renderRow={(rowData) =>
                  <TouchableOpacity onPress={() => this.selectResult(rowData.description)}>
                    <Text style={styles.searchResult}>{rowData.description}</Text>
                  </TouchableOpacity>
                }
              />
            </ScrollView>
          }

          <View style={styles.map}>
            <SampleMap
              region={this.props.position}
              locations = {this.state.markers}
              onRegionChange={(region) => this.handleRegionChange(region)}
            >
            </SampleMap>

            { this.state.loading ?
              <LoadingTopBar color={Colors.primaryColor} size={50}/>
              : null
            }

          </View>

          <View style={styles.mainButtonsContainer}>
            <TouchableOpacity
              onPress={() => this.changeContent("Request") }
              color="#00b7a8"
              style={styles.button}>

              <Text style={styles.buttonText}>Request Kit</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.changeContent("Sampling") }
              color="#00b7a8"
              style={styles.button}>

              <Text style={styles.buttonText}>Begin Sampling</Text>
            </TouchableOpacity>
          </View>

        </View>

    )
  }
}
