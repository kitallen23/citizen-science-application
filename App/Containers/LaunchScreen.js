import React, { Component } from 'react'
import { Alert, Keyboard, KeyboardAvoidingView, ScrollView, Text, Image, View, TextInput, AsyncStorage, ActivityIndicator } from 'react-native'
import TextInputLogin from '../Components/TextInputLogin.js'
import LoginButton from '../Components/LoginButton.js'
import Swiper from 'react-native-swiper';

import { Images } from '../Themes'

// Styles
import styles from './Styles/LaunchScreenStyles'

export default class LaunchScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      skipWelcome: false,
      loginResponse: '',
    };
    this.navigate = this.props.navigation.navigate
  }

  static navigationOptions = {
    title: 'Login',
  }

  submitLogin(callback) {
    fetch('https://citsciapp.herokuapp.com/login', {
      method: 'POST',
      headers:  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {

        if (responseJson.status == "SUCCESS") {
          AsyncStorage.setItem("token", responseJson.token).then(() => {
          AsyncStorage.setItem("refresh_token", responseJson.refresh_token).then(() => {
          AsyncStorage.setItem("account:name", responseJson.name).then(() => {
          AsyncStorage.setItem("account:email", this.state.email).then(() => {
            this.setState({name: responseJson.name})
            callback();
          });
          });
          });
          });
        }
        else {
          this.refs.swiper.scrollBy(-1);
          this.setState({loginResponse: "Incorrect Email/Password Combination"})
        }
    })
    .catch((error) => {
      console.error(error);
    });

  }

  login() {
    Keyboard.dismiss();

    // Move to the 'logging in' view
    this.refs.swiper.scrollBy(1);

    this.submitLogin(this.successLogin.bind(this));
  }

  successLogin() {
    // Deciding which screen to navigate to
    if(this.state.skipWelcome) {
      this.navigate('MainScreen', {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password
      });
    }
    else {
      this.navigate('SwiperScreen', {
        name: this.state.name,
        email: this.state.email,
      });
    }
  }

  componentDidMount() {
    //AUTO LOGIN FOR TESTING
    //DELETE THIS TO USE YOUR OWN INPUT
    // this.setState({email: 'bugs@rubyplus.com'});
    // this.setState({password: '123456'});

    AsyncStorage.getItem("@Settings:skipWelcome")
    .then((value) => {
      if(value != undefined) {
        setting = JSON.parse(value)
        this.setState({skipWelcome: setting})
      }
    });
  }

  render () {

    return (
      <View style={styles.mainContainer} >

        <Swiper style={styles.wrapper} ref="swiper" showsButtons={false} showsPagination={false} loop={false} scrollEnabled={false}>

          {/*** Login form slide ***/}
          <View style={styles.slide1}>

            <Image source={Images.citloginBg} style={styles.background} />

            <KeyboardAvoidingView behavior="padding">

              <View style={styles.centeredHorizontal}>
                <Image source={Images.citscilogo} style={styles.logo} />
              </View>

              <Text style={styles.responseText}>{this.state.loginResponse}</Text>

              <TextInputLogin
                keyboardType="email-address"
                placeholder="Email Address"
                onChangeText={(email) => this.setState({email})}
                value={this.state.email}
                returnKeyType="next"

                onSubmitEditing={(event) => {
                  this.refs.PasswordInput.focus();
                  }}

                />

              <TextInputLogin
                placeholder="Password"
                onChangeText={(password) => this.setState({password})}
                value={this.state.password}
                returnKeyType="go"

                ref='PasswordInput'

                onSubmitEditing={() => this.login()}
                secureTextEntry

                />

              <LoginButton
                onPress={() => this.login()}

                ref='LoginButton'
                />

              <Text
                style={styles.linkTextSmall}
                onPress={() => this.navigate('RegistrationScreen')}>
                Don't have an account? Register here!
              </Text>

            </KeyboardAvoidingView>

          </View>

          {/*** Logging in slide ***/}
          <View style={styles.slide1}>
            <View style={styles.spinnerWrapper}>
              <ActivityIndicator
                size="large"
                color="#fff"
                style={styles.spinner}
              />
            </View>

            <View style={styles.loginTextWrapper}>
              <Text style={styles.centerTextLarge}>Logging in...</Text>
            </View>
          </View>

        </Swiper>

      </View>
    )
  }
}
