import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
  },
  topleft: {
    position: "absolute",
    left: 0,
    width: 50,
    height: 50,
    zIndex: 1,
    backgroundColor: Colors.buttonBackground,
    elevation: 1,
  },
  buttonPadding: {
    padding: 10,
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centeredHorizontal: {
    alignItems: 'center'
  },
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: Colors.windowTint,
  },
  textLarge: {
    color: Colors.windowTint,
    fontSize: 20
  },

  searchResult: {
    fontSize: 18,
    color: "black",
    paddingVertical: 10,
  },
  resultsContainer: {
    position: "absolute",
    top: 70,
    right: 0,
    left: 0,
    padding: 10,
    marginHorizontal: Metrics.marginHorizontal,
    zIndex: 2,
    borderRadius: 5,
    backgroundColor: Colors.snow,
    elevation: 3,
    maxHeight: 250
  },

  mainButtonsContainer: {
    position: "absolute",
    bottom: 30,
    right: 0,
    left: 0,
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row',
    zIndex: 1,
  },
  button: {
    paddingVertical: 15,
    paddingHorizontal: 25,
    margin: 10,
    backgroundColor: Colors.buttonBackground,
    borderRadius: 3,
    elevation: 1,
    width: "40%",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: Colors.snow,
    fontSize: 15,
    textAlign: 'center',
  },
})
