import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MainScreen from './MainScreen.js';
import { changeContent, changeHome, setDrawer, toggleDrawer } from '../../Utils/Navigation/navigation.actions.js';
import { changePosition } from '../../Utils/Map/map.actions.js';

const mapStateToProps = state => ({
  content: state.navigation.content,
  drawer: state.navigation.drawer,
  position: state.map.position,
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ changeContent, changeHome, setDrawer, toggleDrawer, changePosition }, dispatch );
};

export default connect( mapStateToProps, mapDispatchToProps )( MainScreen );
