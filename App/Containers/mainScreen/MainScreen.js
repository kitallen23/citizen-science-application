import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { AsyncStorage, Alert, Keyboard, Text, Image, View, TextInput } from 'react-native'
import RoundedButton from '../../Components/RoundedButton.js'
import SideNav from '../../Components/SideNav.js'

// Add screens here
import ListScreen from '../Content/listScreen'
import MapScreen from '../Content/mapScreen'
import RequestScreen from '../Content/requestScreen'
import SamplingMainScreen from '../Content/samplingScreen'
import SettingsScreen from '../Content/settingsScreen'
import ProfileScreen from '../Content/profileScreen'
import ScanScreen from '../Content/scanScreen'
import HelpScreen from '../Content/helpScreen'
import AboutScreen from '../Content/aboutScreen'
import LegalScreen from '../Content/legalScreen'

// Styles
import { Colors, Images } from '../../Themes/'
import styles from './MainScreenStyles'

export default class MainScreen extends Component {

  static propTypes = {
    content: PropTypes.string,
    position: PropTypes.object,
    changeContent: PropTypes.func,
    changeHome: PropTypes.func,
    openDrawer: PropTypes.func,
    closeDrawer: PropTypes.func,
    toggleDrawer: PropTypes.func,
    changePosition: PropTypes.func,

  }

  static navigationOptions = {
    title: 'Main Menu',
  }

  componentDidMount() {
  }

  showDrawer(){
    this.props.setDrawer(true)
  }
  closeDrawer(){
    this.props.setDrawer(false)
  }
  changeContent(content){
    this.closeDrawer();
    this.props.changeContent(content)
  }
  goHome(){
    this.changeContent(this.props.home)
  }
  goLogin(){
    this.props.navigation.navigate("LaunchScreen")
  }

  mySamplesScreen() {

    AsyncStorage.getItem("@Settings:defaultScreen")
    .then((value) => {
      if(value != undefined) {
        setting = value
        this.changeContent(setting)
      }
      else {
        this.changeContent("Map")
      }
    });

  }

  render () {

    const { navigate } = this.props.navigation;
    const { position } = this.props;

    const content = this.props.content

    return (

      <SideNav
        open={this.props.drawer}
        closeAction={this.closeDrawer.bind(this)}
        onClose={ () => this.closeDrawer() }
        onOpen={ () => Keyboard.dismiss() }
        changeContent={this.changeContent.bind(this)}
        currentContent={content}
        >

        <View style={styles.mainContainer}>
          {(() => {
            switch (content) {
              case 'MySamples':
                this.mySamplesScreen()
              case 'Map':
                return <MapScreen
                  location={position}
                  getContent={content}
                  goLogin={this.goLogin.bind(this)}
                  />
              case 'List':
                return <ListScreen
                  goLogin={this.goLogin.bind(this)}
                  />
              case 'Request':
                return <RequestScreen
                  goLogin={this.goLogin.bind(this)}
                  />
              case 'Sampling':
                return <SamplingMainScreen
                  goLogin={this.goLogin.bind(this)}
                  />
              case 'Scan':
                return <ScanScreen
                  goLogin={this.goLogin.bind(this)}
                  />
              case 'Settings':
                return <SettingsScreen
                  goLogin={this.goLogin.bind(this)}
                  />
              case 'Profile':
                return <ProfileScreen
                  goLogin={this.goLogin.bind(this)}
                  />
              case 'Help':
                return <HelpScreen
                  />
              case 'About':
                return <AboutScreen
                  />
              case 'Legal':
                return <LegalScreen
                  />
              default :
                null
            }
          })()}

        </View>
      </SideNav>

    )
  }
}
