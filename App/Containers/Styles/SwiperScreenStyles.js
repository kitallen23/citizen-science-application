import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centeredHorizontal: {
    alignItems: 'center'
  },
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: 'white'
  },
  textLarge: {
    color: 'white',
    fontSize: 20
  },
  image: {
    height: "50%",
    width: "75%",
  },

  /* Swiper */
  wrapper: {
  },
  slide1: {
    flex: 1,
    padding: 30,
    justifyContent: 'center',
    backgroundColor: Colors.primaryDarkerColor,
    width: "100%"
  },
  slide2: {
    flex: 1,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primaryDarkerColor,
  },
  slide3: {
    flex: 1,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primaryDarkerColor,

    width: "100%"
  },
  slide4: {
    flex: 1,
    padding: 30,
    justifyContent: 'center',
    backgroundColor: Colors.primaryDarkerColor,
  },
  slide5: {
    flex: 1,
    padding: 30,
    justifyContent: 'center',
    backgroundColor: Colors.primaryDarkerColor,
  },
  slideText: {
    color: '#fff',
    marginBottom: 15,
    fontSize: Fonts.size.h3,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  slideTextSmall: {
    color: '#fff',
    fontSize: Fonts.size.normal,
    textAlign: 'center',
  },
  slideTextSmallLeft: {
    color: '#fff',
    fontSize: Fonts.size.normal,
    textAlign: 'left',
  },
  beginButton: {
    alignItems: 'center',
    marginHorizontal: 50,
    padding: Metrics.spacing.medium,
    borderRadius: 5,
    backgroundColor: Colors.primaryColor,
    elevation: 1,

  },
  buttonContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonText: {
    color: Colors.snow,
    fontSize: Fonts.size.large,
    fontWeight: "bold",
    marginHorizontal: Metrics.spacing.medium,
  },
})
