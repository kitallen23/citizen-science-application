import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: 0,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  responseText: {
    color: Colors.snow,
    textAlign: "center",
    fontWeight: 'bold',
  },
  logo: {
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain',
    marginVertical: 0
  },
  centeredHorizontal: {
    alignItems: 'center',
  },
  linkTextSmall: {
    color: '#fff',
    fontSize: Fonts.size.large,
    textAlign: 'center'
  },
  centerTextLarge: {
    color: '#fff',
    fontSize: Fonts.size.h4,
    textAlign: 'center'
  },
  spinner: {
    transform: [
      {scale: 1.5}
    ]
  },
  spinnerWrapper: {
    flex: 2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 45,
  },
  loginTextWrapper: {
    flex: 3,
  },
  /* Slider */
  slide1: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.primaryColor,
    width: "100%"
  },
  slide2: {
    padding: 30,
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: Colors.primaryColor,
  },
})
