import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: 0,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  responseText: {
    color: Colors.snow,
    textAlign: "center",
    color: '#FFAB91',
    fontSize: Fonts.size.normal,
    fontWeight: 'bold',
    marginBottom: Metrics.spacing.xl,
  },
  logo: {
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain',
    marginVertical: 0
  },
  centeredHorizontal: {
    alignItems: 'center',
    backgroundColor: Colors.transparent,
  },
  linkTextSmall: {
    color: '#fff',
    fontSize: Fonts.size.normal,
    textAlign: 'center',
    marginHorizontal: Metrics.loginItems,
  },
  centerTextLarge: {
    color: '#fff',
    fontSize: Fonts.size.large,
    textAlign: 'center'
  },
  spinner: {
    transform: [
      {scale: 1.5}
    ]
  },
  spinnerWrapper: {
    flex: 2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 45,
  },
  loginTextWrapper: {
    flex: 3,
  },
  formRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    marginTop: 10,
    marginHorizontal: Metrics.loginItems,
  },
  formRowElement: {
    flex: 1,
  },
  textInput: {
    backgroundColor: Colors.snow,
    height: 45,
    borderColor: Colors.transparent,
    borderRadius: 5,
    borderWidth: 2,
    fontSize: 15,
    paddingRight: 20,
    paddingLeft: 20,
  },
  statePickerWrapper: {
    height: 45,
    borderColor: Colors.transparent,
    borderRadius: 5,
    borderWidth: 2,
    backgroundColor: 'white',
  },
  statePicker: {
    padding: 0,
    height: 40,
  },
  iconButton: {
    flex: 1,
    alignItems: 'center',
    height: 45,
    marginHorizontal: Metrics.loginItems,
    marginVertical: Metrics.baseMargin,
    backgroundColor: Colors.transparent,
    justifyContent: 'center',
    borderColor: Colors.transparent,
    borderRadius: 5,
    borderWidth: 2,
    padding: 20,
  },
  buttonText: {
    color: Colors.snow,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.large,
    marginVertical: Metrics.baseMargin,
  },
  buttonContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    marginLeft: 6,
  },
  /* Slider */
  slide1: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.primaryColor,
    width: "100%"
  },
  slideText: {
    color: '#fff',
    marginBottom: 15,
    fontSize: 30,
    textAlign: 'center',
    fontWeight: 'bold',
  },
})
