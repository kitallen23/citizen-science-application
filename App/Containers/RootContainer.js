import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import { connect } from 'react-redux'
import ReduxPersist from '../Config/ReduxPersist'
import Colors from '../Themes/Colors'

// Styles
import styles from './Styles/RootContainerStyles'

class RootContainer extends Component {
  componentDidMount () {
  }

  render () {
    return (
      <View style={styles.applicationView}>
        <StatusBar
            backgroundColor={Colors.statusbar}
            barStyle="light-content"
          />
        <ReduxNavigation />
      </View>
    )
  }
}

export default RootContainer
